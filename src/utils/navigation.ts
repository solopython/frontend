import {
  BookmarkSquareIcon,
  ComputerDesktopIcon,
  GlobeAltIcon,
  ShoppingBagIcon,
  TagIcon,
  UserGroupIcon,
} from '@heroicons/react/20/solid';
import { AcademicCapIcon } from '@heroicons/react/24/outline';

export const products = [
  {
    name: 'Online Courses',
    description:
      'Descubre nuestros cursos interactivos y a tu ritmo para aprender programacion desde cero.',
    href: '/courses',
    icon: AcademicCapIcon,
  },
];

export const callsToAction = [
  { name: 'Tienda en Linea', href: '/store', icon: ShoppingBagIcon },
  { name: 'Precios', href: '/contact', icon: TagIcon },
];

export const company = [
  { name: 'Community', href: '#', icon: UserGroupIcon },
  { name: 'Blog', href: '#', icon: GlobeAltIcon },
  { name: 'Guides', href: '#', icon: BookmarkSquareIcon },
  { name: 'Webinars', href: '#', icon: ComputerDesktopIcon },
];

export const community = [
  { name: 'Discord', href: 'https://discord.gg/BcxQwhXjZh' },
  { name: 'Blog', href: '/blog' },
  { name: 'Videos', href: '/videos' },
  { name: 'Recursos', href: '/resources' },
];
