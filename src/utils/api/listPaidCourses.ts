export interface FetchPaidCoursesProps {
  p: number;
  page_size: number;
  max_page_size: number;
  sorting: string;
  order: string;
  category: string;
  author: string;
  search: string;
  levels: string;
}

export async function fetchPaidCourses({
  p,
  page_size,
  max_page_size,
  sorting,
  order,
  category,
  author,
  search,
  levels,
}: FetchPaidCoursesProps) {
  const res = await fetch(
    `/api/courses/list_paid/?p=${p}&page_size=${page_size}&max_page_size=${max_page_size}&sorting=${sorting}&order=${order}&category=${category}&author=${author}&search=${search}&levels=${levels}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
  );
  const data = await res.json();
  return data;
}
