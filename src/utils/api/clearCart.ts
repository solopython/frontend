export async function fetchClearCart() {
  const res = await fetch('/api/cart/clear', {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  const data = await res.json();
  return data;
}
