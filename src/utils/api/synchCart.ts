export interface SynchCartProps {
  items: any;
}

export async function synchCart({ items }: SynchCartProps) {
  const body = JSON.stringify({
    items,
  });

  const res = await fetch(`/api/cart/synch`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });
  const data = await res.json();
  return data.results;
}
