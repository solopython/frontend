export interface FetchEpisodeCommentsProps {
  p: number;
  page_size: number;
  max_page_size: number;
  episode_id: string;
}

export async function fetchEpisodeComments({
  p,
  page_size,
  max_page_size,
  episode_id,
}: FetchEpisodeCommentsProps) {
  const res = await fetch(
    `/api/courses/sections/episodes/comments/list/?p=${p}&page_size=${page_size}&max_page_size=${max_page_size}&episode_id=${episode_id}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
  );
  const data = await res.json();
  return data;
}
