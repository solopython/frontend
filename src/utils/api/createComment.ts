export interface CreateCommentProps {
  p: number;
  page_size: number;
  max_page_size: number;
  content: string;
  episode_id: string;
}

export async function fetchCreateComment({
  p,
  page_size,
  max_page_size,
  content,
  episode_id,
}: CreateCommentProps) {
  const body = JSON.stringify({
    p,
    page_size,
    max_page_size,
    content,
    episode_id,
  });

  const res = await fetch(`/api/courses/sections/episodes/comments/create/`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });
  const data = await res.json();
  return data;
}
