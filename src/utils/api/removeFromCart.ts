export interface RemoveFromCartProps {
  itemID: string | number;
  type: string;
}

export async function removeFromCart({ itemID, type }: RemoveFromCartProps) {
  const body = JSON.stringify({
    itemID,
    type,
  });

  const res = await fetch('/api/cart/remove', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });
  const data = await res.json();
  return data.results;
}
