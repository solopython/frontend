export interface EditCommentProps {
  p: number;
  page_size: number;
  max_page_size: number;
  content: string;
  comment_id: string;
}

export async function fetchEditComment({
  p,
  page_size,
  max_page_size,
  content,
  comment_id,
}: EditCommentProps) {
  const body = JSON.stringify({
    p,
    page_size,
    max_page_size,
    content,
    comment_id,
  });

  const res = await fetch(`/api/courses/sections/episodes/comments/edit/`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });
  const data = await res.json();
  return data;
}
