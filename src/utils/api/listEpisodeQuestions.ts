export interface FetchEpisodeQuestionsProps {
  p: number;
  page_size: number;
  max_page_size: number;
  episode_id: string;
  search: string;
  filter_by: string;
  order_by: string;
}

export async function fetchEpisodeQuestions({
  p,
  page_size,
  max_page_size,
  episode_id,
  search,
  filter_by,
  order_by,
}: FetchEpisodeQuestionsProps) {
  const res = await fetch(
    `/api/courses/sections/episodes/questions/list/?p=${p}&page_size=${page_size}&max_page_size=${max_page_size}&episode_id=${episode_id}&search=${search}$filter_by=${filter_by}&order_by=${order_by}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
  );
  const data = await res.json();
  return data;
}
