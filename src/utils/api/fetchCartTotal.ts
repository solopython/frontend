export async function fetchCartTotal(items) {
  const body = JSON.stringify({
    items,
  });
  const res = await fetch(`/api/cart/total`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });
  const data = await res.json();
  return data.results;
}
