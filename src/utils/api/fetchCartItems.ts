export async function fetchCartItems() {
  const cartItemsRes = await fetch(`/api/cart/fetch`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  const data = await cartItemsRes.json();
  return data.results;
}
