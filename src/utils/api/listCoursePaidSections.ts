export interface FetchCoursePaidSectionsProps {
  p: number;
  page_size: number;
  max_page_size: number;
  slug: string;
}

export async function fetchCoursePaidSections({
  p,
  page_size,
  max_page_size,
  slug,
}: FetchCoursePaidSectionsProps) {
  const res = await fetch(
    `/api/courses/sections/list/?p=${p}&page_size=${page_size}&max_page_size=${max_page_size}&slug=${slug}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
  );
  const data = await res.json();
  return data;
}
