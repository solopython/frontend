export interface CreateQuestionProps {
  p: number;
  page_size: number;
  max_page_size: number;
  title: string;
  content: string;
  episode_id: string;
}

export async function fetchCreateQuestion({
  p,
  page_size,
  max_page_size,
  title,
  content,
  episode_id,
}: CreateQuestionProps) {
  const body = JSON.stringify({
    p,
    page_size,
    max_page_size,
    title,
    content,
    episode_id,
  });

  const res = await fetch(`/api/courses/sections/episodes/questions/create/`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });
  const data = await res.json();
  return data;
}
