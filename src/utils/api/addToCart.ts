export interface AddToCartProps {
  itemID: string | number;
  type: string;
  coupon: any | null;
  shipping: string | null;
  quantity: number | null;
  size: string | null;
  color: string | null;
  weight: string | null;
  material: string | null;
  referrer: string | null;
}

export async function addToCart({
  itemID,
  type,
  coupon,
  shipping,
  quantity,
  size,
  color,
  weight,
  material,
  referrer,
}: AddToCartProps) {
  const body = JSON.stringify({
    itemID,
    type,
    shipping: shipping || '',
    color: color || '',
    size: size || '',
    weight: weight || '',
    material: material || '',
    quantity,
    coupon,
    referrer,
  });

  const res = await fetch('/api/cart/add', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });

  const data = await res.json();
  return data.results;
}
