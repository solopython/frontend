export interface DeleteCommentProps {
  p: number;
  page_size: number;
  max_page_size: number;
  comment_id: string;
}

export async function fetchDeleteComment({
  p,
  page_size,
  max_page_size,
  comment_id,
}: DeleteCommentProps) {
  const body = JSON.stringify({
    p,
    page_size,
    max_page_size,
    comment_id,
  });

  const res = await fetch(`/api/courses/sections/episodes/comments/delete/`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  });
  const data = await res.json();
  return data;
}
