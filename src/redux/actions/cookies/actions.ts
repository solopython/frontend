import { ACCEPT_COOKIES, REJECT_COOKIES } from './types';
import { Dispatch } from 'redux';

export const acceptCookies = () => async (dispatch: Dispatch) => {
  dispatch({
    type: ACCEPT_COOKIES,
  });
};

export const rejectCookies = () => async (dispatch: Dispatch) => {
  dispatch({
    type: REJECT_COOKIES,
  });
};
