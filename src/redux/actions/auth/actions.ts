import { Dispatch } from 'redux';
import {
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  SET_AUTH_LOADING,
  REMOVE_AUTH_LOADING,
  LOGOUT_SUCCESS,
  LOGOUT_FAIL,
  ACTIVATION_SUCCESS,
  ACTIVATION_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  USER_LOADED_SUCCESS,
  USER_LOADED_FAIL,
  RESET_LOGIN_SUCCESS,
  OPEN_LOGIN_MODAL,
  CLOSE_LOGIN_MODAL,
} from './types';
import { ToastError } from '@/components/toast/ToastError';
import { ToastSuccess } from '@/components/toast/ToastSuccess';
import { IActivationProps, ILoginProps, IRegisterProps } from './interface';
import { synchCartAuthenticated } from '../cart/actions';

export const register =
  ({ first_name, last_name, email, username, password, re_password, agreed }: IRegisterProps) =>
  async (dispatch: Dispatch) => {
    dispatch({
      type: SET_AUTH_LOADING,
    });

    const body = JSON.stringify({
      first_name,
      last_name,
      email,
      username,
      password,
      re_password,
      agreed,
    });

    try {
      const res = await fetch('/api/auth/register', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body,
      });

      if (res.status === 201) {
        dispatch({
          type: SIGNUP_SUCCESS,
        });
        ToastSuccess('We have sent you an email, please click the link to verify your account.');
      } else {
        dispatch({
          type: SIGNUP_FAIL,
        });
        ToastSuccess('Check your email and if your username already exists try to login.');
      }
    } catch (err: any) {
      dispatch({
        type: SIGNUP_FAIL,
      });
      ToastError(err?.request?.response);
      dispatch({
        type: REMOVE_AUTH_LOADING,
      });
    }

    dispatch({
      type: REMOVE_AUTH_LOADING,
    });
  };

export const resetRegister = () => async (dispatch: Dispatch) => {
  dispatch({
    type: SIGNUP_FAIL,
  });
};

export const loginCompleted = () => async (dispatch: Dispatch) => {
  dispatch({
    type: RESET_LOGIN_SUCCESS,
  });
};
export const openLoginModal = () => async (dispatch: Dispatch) => {
  dispatch({
    type: OPEN_LOGIN_MODAL,
  });
};
export const closeLoginModal = () => async (dispatch: Dispatch) => {
  dispatch({
    type: CLOSE_LOGIN_MODAL,
  });
};

export const activate =
  ({ uid, token }: IActivationProps) =>
  async (dispatch: Dispatch) => {
    dispatch({
      type: SET_AUTH_LOADING,
    });

    const body = JSON.stringify({
      uid,
      token,
    });

    try {
      const res = await fetch('/api/auth/activate', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body,
      });

      if (res.status === 204) {
        dispatch({
          type: ACTIVATION_SUCCESS,
        });
        ToastSuccess('Your account has been activated, you may now login.');
      } else {
        dispatch({
          type: ACTIVATION_FAIL,
        });
        ToastError('Error activating account.');
      }
    } catch (err: any) {
      dispatch({
        type: SIGNUP_FAIL,
      });
      ToastError(err?.request?.response);
      dispatch({
        type: REMOVE_AUTH_LOADING,
      });
    }

    dispatch({
      type: REMOVE_AUTH_LOADING,
    });
  };

export const resetActivation = () => async (dispatch: Dispatch) => {
  dispatch({
    type: ACTIVATION_FAIL,
  });
};

export const loadUser = () => async (dispatch: Dispatch) => {
  try {
    const res = await fetch(`/api/auth/user`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    const data = await res.json();

    if (res.status === 200) {
      dispatch({
        type: USER_LOADED_SUCCESS,
        payload: data.user,
      });
    } else {
      dispatch({
        type: USER_LOADED_FAIL,
      });
    }
  } catch (err) {
    dispatch({
      type: USER_LOADED_FAIL,
    });
  }
};

export const login =
  ({ email, password }: ILoginProps) =>
  async (dispatch: any) => {
    const body = JSON.stringify({
      email,
      password,
    });

    dispatch({
      type: SET_AUTH_LOADING,
    });

    try {
      const res = await fetch(`/api/auth/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      });

      if (res.status === 200) {
        dispatch({
          type: LOGIN_SUCCESS,
        });
        dispatch(loadUser());
      } else {
        dispatch({
          type: LOGIN_FAIL,
        });
        ToastError('Wrong email or password.');
      }
    } catch (err) {
      dispatch({
        type: LOGIN_FAIL,
      });
      ToastError('Wrong email or password.');
    }

    dispatch({
      type: REMOVE_AUTH_LOADING,
    });
  };

export const logout = () => async (dispatch: Dispatch) => {
  try {
    const res = await fetch('/api/auth/logout', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
      },
    });

    if (res.status === 200) {
      dispatch({
        type: LOGOUT_SUCCESS,
      });
      // localStorage.removeItem('persist:root', accounts[0]);
    } else {
      dispatch({
        type: LOGOUT_FAIL,
      });
    }
  } catch (err) {
    dispatch({
      type: LOGOUT_FAIL,
    });
  }
};
