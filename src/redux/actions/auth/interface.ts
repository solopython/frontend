export interface IRegisterProps {
  first_name: string;
  last_name: string;
  email: string;
  username: string;
  password: string;
  re_password: string;
  agreed: boolean;
}

export interface IActivationProps {
  uid: string | null;
  token: string | null;
}

export interface ILoginProps {
  email: string;
  password: string;
}
