import { GET_PAID_COURSES_ID_LIST_SUCCESS, GET_PAID_COURSES_ID_LIST_FAIL } from './types';
import { Dispatch } from 'redux';

export const listPaidCoursesById = () => async (dispatch: Dispatch) => {
  try {
    const res = await fetch('/api/courses/list_paid_by_id');

    const data = await res.json();

    if (res.status === 200) {
      dispatch({
        type: GET_PAID_COURSES_ID_LIST_SUCCESS,
        payload: data.results,
      });
    } else {
      dispatch({
        type: GET_PAID_COURSES_ID_LIST_FAIL,
      });
    }
  } catch (e) {
    dispatch({
      type: GET_PAID_COURSES_ID_LIST_FAIL,
    });
  }
};
