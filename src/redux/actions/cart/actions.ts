import axios from 'axios';
import { Dispatch } from 'redux';
import { ICartItem } from '@/interfaces/cart/CartItem';
import {
  GET_ITEMS_SUCCESS,
  GET_ITEMS_FAIL,
  REMOVE_ITEM_SUCCESS,
  REMOVE_ITEM_FAIL,
  GET_TOTAL_SUCCESS,
  GET_TOTAL_FAIL,
  ADD_ITEM,
  EMPTY_CART_SUCCESS,
  EMPTY_CART_FAIL,
  SYNCH_CART_SUCCESS,
  SYNCH_CART_FAIL,
} from './types';
import {
  AddToCartAnonymousProps,
  AddToCartProps,
  RemoveFromCartProps,
  RemoveFromCartAnonymousProps,
  SynchCartProps,
} from './interfaces';
import { loginCompleted } from '../auth/actions';

export const getItems = () => async (dispatch: Dispatch) => {
  try {
    const res = await fetch('/api/cart/fetch');

    const data = await res.json();

    if (res.status === 200) {
      dispatch({
        type: GET_ITEMS_SUCCESS,
        payload: data.results,
      });
    } else {
      dispatch({
        type: GET_ITEMS_FAIL,
      });
    }
  } catch (err) {
    dispatch({
      type: GET_ITEMS_FAIL,
    });
  }
};

export const getCartTotal = (items: ICartItem[]) => async (dispatch: Dispatch) => {
  // Send an empty array if the body is empty
  const body = JSON.stringify({
    items,
  });

  try {
    const res = await fetch(`/api/cart/total`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body,
    });

    const data = await res.json();

    if (res.status === 200) {
      dispatch({
        type: GET_TOTAL_SUCCESS,
        payload: data.results,
      });
    } else {
      dispatch({
        type: GET_TOTAL_FAIL,
      });
    }
  } catch (err) {
    dispatch({
      type: GET_TOTAL_FAIL,
    });
  }
};

export const addCartItem =
  ({
    itemID,
    type,
    coupon,
    shipping,
    quantity,
    size,
    color,
    weight,
    material,
    referrer,
  }: AddToCartProps) =>
  async (dispatch: Dispatch) => {
    try {
      const body = JSON.stringify({
        itemID,
        type,
        shipping: shipping || '',
        color: color || '',
        size: size || '',
        weight: weight || '',
        material: material || '',
        quantity,
        coupon,
        referrer,
      });

      const res = await fetch('/api/cart/add', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body,
      });

      const data = await res.json();

      if (res.status === 201) {
        dispatch({
          type: GET_ITEMS_SUCCESS,
          payload: data.results,
        });
        dispatch(getCartTotal(data.results.cart));
      } else {
        dispatch({
          type: GET_ITEMS_FAIL,
        });
      }
    } catch (err) {
      dispatch({
        type: GET_ITEMS_FAIL,
      });
    }
  };

export const addCartItemAnonymous =
  ({
    item,
    type,
    coupon,
    shipping,
    quantity,
    size,
    color,
    weight,
    material,
    referrer,
  }: AddToCartAnonymousProps) =>
  (dispatch: Dispatch) => {
    let cart = [];

    if (localStorage.getItem('cart')) {
      cart = JSON.parse(localStorage.getItem('cart'));
    }

    let updatedCart = [...cart]; // Make a copy of the cart array

    if (type === 'Course') {
      console.log('"Adding Course to Cart');

      // const courseExists = updatedCart.find((item: any) => item?.course.id === item.id);
      const courseItem = {
        course: {
          id: item?.id,
          title: item?.title,
          slug: item?.slug,
          short_description: item?.short_description,
          price: item?.price,
          thumbnail: item?.images[0].file,
        },
        type: 'Course',
        coupon,
        referrer: '',
      };

      updatedCart.push(courseItem);

      dispatch(getCartTotal(updatedCart));
      // if (!courseExists) {
      // }
    }

    if (type === 'Tier') {
      console.log('"Adding Tier to Cart');
      // const tierExists = updatedCart.find((item: any) => item?.tier.id === item.id);
      const tierItem = {
        tier: {
          id: item?.id,
          title: item?.title,
          description: item?.description,
          reason: item?.reason,
          transaction_amount: item?.transaction_amount,
          thumbnail: item?.thumbnail,
        },
        type: 'Tier',
        coupon,
        referrer: '',
      };

      // if (!tierExists) {
      // }
      updatedCart.push(tierItem);

      dispatch(getCartTotal(updatedCart));
    }

    const results = {
      cart: updatedCart,
      total_items: updatedCart.length,
    };

    localStorage.setItem('cart', JSON.stringify(updatedCart)); // Update localStorage with the modified cart

    dispatch({
      type: GET_ITEMS_SUCCESS,
      payload: results,
    });
  };

export const removeCartItem =
  ({ itemID, type }: RemoveFromCartProps) =>
  async (dispatch: Dispatch) => {
    const body = JSON.stringify({ itemID, type });

    try {
      const res = await fetch('/api/cart/remove', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body,
      });

      const data = await res.json();

      if (res.status === 200) {
        dispatch({
          type: GET_ITEMS_SUCCESS,
          payload: data.results,
        });
        dispatch(getCartTotal(data.results.cart));
      } else {
        dispatch({
          type: GET_ITEMS_FAIL,
        });
      }
    } catch (err) {
      dispatch({
        type: GET_ITEMS_FAIL,
      });
    }
  };

export const removeCartItemAnonymous =
  ({ item, type }: RemoveFromCartAnonymousProps) =>
  (dispatch: Dispatch) => {
    let cart = [];

    if (localStorage.getItem('cart')) {
      cart = JSON.parse(localStorage.getItem('cart'));
    }

    // Filter out the item to be removed based on type and id
    if (type === 'Course') {
      // Add the logic to remove course items if needed
      let newCart = cart.filter((cartItem: any) => {
        if (type === 'Course' && cartItem.type === 'Course') {
          return cartItem.course.id !== item.id;
        }
        return true; // Keep items that do not match the specified type
      });

      // Store updated cart back in localStorage
      localStorage.setItem('cart', JSON.stringify(newCart));

      const results = {
        cart: newCart,
        total_items: newCart.length,
      };

      dispatch(getCartTotal(newCart));

      dispatch({
        type: ADD_ITEM,
        payload: results,
      });
    }

    if (type === 'Tier') {
      let newCart = cart.filter((cartItem: any) => {
        if (type === 'Tier' && cartItem.type === 'Tier') {
          return cartItem.tier.id !== item.id;
        }
        return true; // Keep items that do not match the specified type
      });

      // Store updated cart back in localStorage
      localStorage.setItem('cart', JSON.stringify(newCart));

      const results = {
        cart: newCart,
        total_items: newCart.length,
      };

      dispatch(getCartTotal(newCart));

      dispatch({
        type: ADD_ITEM,
        payload: results,
      });
    }
  };

export const emptyCartAuthenticated = () => async (dispatch: Dispatch) => {
  try {
    const res = await fetch('/api/cart/clear', { method: 'GET' });

    if (res.status === 200) {
      dispatch({
        type: EMPTY_CART_SUCCESS,
      });
    } else {
      dispatch({
        type: EMPTY_CART_FAIL,
      });
    }
  } catch (err) {
    dispatch({
      type: EMPTY_CART_FAIL,
    });
  }
};

export const emptyCartAnonymous = () => (dispatch: Dispatch) => {
  dispatch({
    type: EMPTY_CART_SUCCESS,
  });
};

export const synchCartAuthenticated =
  ({ items }: SynchCartProps) =>
  async (dispatch: Dispatch) => {
    const body = JSON.stringify({
      items,
    });

    try {
      const res = await fetch('/api/cart/synch', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      });

      if (res.status === 200) {
        dispatch({
          type: SYNCH_CART_SUCCESS,
        });
      } else {
        dispatch({
          type: SYNCH_CART_FAIL,
        });
      }
    } catch (err) {
      dispatch({
        type: SYNCH_CART_FAIL,
      });
    }
  };
