export interface AddToCartProps {
  itemID: string | number;
  type: string;
  coupon: any | null;
  shipping: string | null;
  quantity: number | null;
  size: string | null;
  color: string | null;
  weight: string | null;
  material: string | null;
  referrer: string | null;
}

export interface AddToCartAnonymousProps {
  item: any;
  type: string;
  coupon: any | null;
  shipping: string | null;
  quantity: number | null;
  size: string | null;
  color: string | null;
  weight: string | null;
  material: string | null;
  referrer: string | null;
}

export interface RemoveFromCartProps {
  itemID: string | number;
  type: string;
}

export interface RemoveFromCartAnonymousProps {
  item: any;
  type: string;
}

export interface SynchCartProps {
  items: any[];
}
