import { configureStore, ThunkMiddleware } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import { createWrapper } from 'next-redux-wrapper';
import rootReducer, { RootState } from './reducers';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const makeStore = () => {
  const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk as ThunkMiddleware<RootState>],
    devTools: process.env.NODE_ENV !== 'production',
  });

  let persistor = persistStore(store);
  // Add persistor to the store
  (store as any).__persistor = persistor;

  return store;
};

const wrapper = createWrapper(makeStore, { debug: false });

export default wrapper;
