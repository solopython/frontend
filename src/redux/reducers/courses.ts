/* eslint-disable default-param-last */
import {
  GET_PAID_COURSES_ID_LIST_SUCCESS,
  GET_PAID_COURSES_ID_LIST_FAIL,
} from '../actions/courses/types';

type Action = {
  type: string;
  payload?: any;
};

type State = {
  paid_courses_by_id: any[];
};

const initialState = {
  paid_courses_by_id: [],
};

export default function courses(state: State = initialState, action: Action) {
  // eslint-disable-next-line
  const { type, payload } = action;

  switch (type) {
    case GET_PAID_COURSES_ID_LIST_SUCCESS:
      return {
        ...state,
        paid_courses_by_id: payload,
      };
    case GET_PAID_COURSES_ID_LIST_FAIL:
      return {
        ...state,
        paid_courses_by_id: [],
      };
    default:
      return state;
  }
}
