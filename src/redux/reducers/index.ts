import { combineReducers } from 'redux';
import cookies from './cookies';
import auth from './auth';
import cart from './cart';
import courses from './courses';

const rootReducer = combineReducers({ cookies, auth, cart, courses });

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
