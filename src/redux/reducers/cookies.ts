/* eslint-disable default-param-last */
import { ACCEPT_COOKIES, REJECT_COOKIES } from '../actions/cookies/types';

type State = {
  accepted: boolean | null;
};

type Action = {
  type: string;
  payload?: any;
};

const initialState = {
  accepted: null,
};

export default function cookies(state: State = initialState, action: Action) {
  // eslint-disable-next-line
  const { type, payload } = action;

  switch (type) {
    case ACCEPT_COOKIES:
      return {
        ...state,
        cookies: true,
      };
    case REJECT_COOKIES:
      return {
        ...state,
        cookies: false,
      };
    default:
      return state;
  }
}
