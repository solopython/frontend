import {
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  SET_AUTH_LOADING,
  REMOVE_AUTH_LOADING,
  ACTIVATION_SUCCESS,
  ACTIVATION_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  USER_LOADED_SUCCESS,
  USER_LOADED_FAIL,
  LOGOUT_FAIL,
  LOGOUT_SUCCESS,
  RESET_LOGIN_SUCCESS,
  OPEN_LOGIN_MODAL,
  CLOSE_LOGIN_MODAL,
} from '../actions/auth/types';

type Action = {
  type: string;
  payload?: any;
};

type State = {
  user: any | null;
  access: string | null;
  isAuthenticated: boolean;
  loading: boolean;
  user_loading: boolean;
  isLoginCompleted: boolean;
  openLoginModal: boolean;
};

const initialState = {
  user: null,
  access: null,
  isAuthenticated: false,
  loading: false,
  user_loading: false,
  isLoginCompleted: false,
  openLoginModal: false,
};

export default function auth(state: State = initialState, action: Action) {
  const { type, payload } = action;

  switch (type) {
    case SET_AUTH_LOADING:
      return {
        ...state,
        loading: true,
      };
    case REMOVE_AUTH_LOADING:
      return {
        ...state,
        loading: false,
      };
    case OPEN_LOGIN_MODAL:
      return {
        ...state,
        openLoginModal: true,
      };
    case CLOSE_LOGIN_MODAL:
      return {
        ...state,
        openLoginModal: false,
      };
    case RESET_LOGIN_SUCCESS:
      return {
        ...state,
        isLoginCompleted: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        isLoginCompleted: true,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isAuthenticated: false,
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        register_success: true,
      };
    case SIGNUP_FAIL:
      return {
        ...state,
        register_success: false,
      };
    case ACTIVATION_SUCCESS:
      return {
        ...state,
        activation_success: true,
      };
    case ACTIVATION_FAIL:
      return {
        ...state,
        activation_success: false,
      };

    case USER_LOADED_SUCCESS:
      return {
        ...state,
        user: payload,
        user_loading: false,
      };
    case USER_LOADED_FAIL:
      return {
        ...state,
        user: null,
        user_loading: false,
      };
    case LOGOUT_FAIL:
      return {
        ...state,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };
    default:
      return state;
  }
}
