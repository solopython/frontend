import {
  GET_ITEMS_SUCCESS,
  GET_ITEMS_FAIL,
  GET_TOTAL_SUCCESS,
  GET_TOTAL_FAIL,
  ADD_ITEM,
  EMPTY_CART_SUCCESS,
  EMPTY_CART_FAIL,
  SYNCH_CART_SUCCESS,
  SYNCH_CART_FAIL,
} from '../actions/cart/types';

type Action = {
  type: string;
  payload?: any;
};

type State = {
  items: any[];
  courses: any[];
  products: any[];
  tiers: any[];
  amount: number;
  compare_amount: number;
  shipping_estimate: number;
  tax_estimate: number;
  total_compare_cost: number;
  total_cost: number;
  finalPrice: number;
  total_cost_ethereum: number;
  maticCost: number;
  total_items: number;
  open: boolean;
  checkout_loading: boolean;
};

const initialState = {
  items: [],
  courses: [],
  products: [],
  tiers: [],
  amount: 0.0,
  compare_amount: 0.0,
  shipping_estimate: 0.0,
  tax_estimate: 0.0,
  total_compare_cost: 0.0,
  total_cost: 0.0,
  finalPrice: 0.0,
  total_cost_ethereum: 0.0,
  maticCost: 0.0,
  total_items: 0,
  open: false,
  checkout_loading: false,
};

export default function cart(state: State = initialState, action: Action) {
  const { type, payload } = action;

  switch (type) {
    case GET_ITEMS_SUCCESS:
      return {
        ...state,
        items: payload.cart,
        total_items: payload.total_items,
      };
    case GET_ITEMS_FAIL:
      return {
        ...state,
        items: null,
        total_items: null,
      };
    case GET_TOTAL_SUCCESS:
      return {
        ...state,
        amount: payload.total_cost,
        total_cost_ethereum: payload.total_cost_ethereum,
        maticCost: payload.maticCost,
        finalPrice: payload.finalPrice,
        compare_amount: payload.total_compare_cost,
        tax_estimate: payload.tax_estimate,
        shipping_estimate: payload.shipping_estimate,
      };
    case GET_TOTAL_FAIL:
      return {
        ...state,
        amount: 0.0,
        compare_amount: 0.0,
        tax_estimate: 0.0,
        shipping_estimate: 0.0,
        total_cost_ethereum: 0.0,
        finalPrice: 0.0,
      };
    case ADD_ITEM:
      localStorage.setItem('cart', JSON.stringify(payload.cart));
      return {
        ...state,
        items: JSON.parse(localStorage.getItem('cart')),
        total_items: payload.total_items,
      };
    case EMPTY_CART_SUCCESS:
    case EMPTY_CART_FAIL:
      localStorage.setItem('cart', JSON.stringify([]));

      return {
        ...state,
        items: null,
        amount: 0.0,
        compare_amount: 0.0,
        tax_estimate: 0.0,
        shipping_estimate: 0.0,
        total_cost_ethereum: 0.0,
        finalPrice: 0.0,
        total_items: 0,
      };
    case SYNCH_CART_SUCCESS:
    case SYNCH_CART_FAIL:
      localStorage.setItem('cart', JSON.stringify([]));
      return {
        ...state,
      };
    default:
      return state;
  }
}
