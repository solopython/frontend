import { ToastError } from '@/components/toast/ToastError';
import { ToastSuccess } from '@/components/toast/ToastSuccess';
import { useEffect, useState } from 'react';
import Button from '@/components/Button';
import { NewsletterSignupProps, newsletterSignup } from '@/utils/api/newsletterSignup';

export default function EmailForm() {
  const [placeholder, setPlaceholder] = useState('');
  const [email, setEmail] = useState('');

  useEffect(() => {
    setPlaceholder('e.g yourname@email.com');
  }, []);

  const handleSubscribe = async (event: any) => {
    event.preventDefault();

    const emailInput = email;
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (emailInput.trim() === '') {
      // Input is empty
      ToastError('Email is required.');
      return;
    }

    if (!emailRegex.test(emailInput)) {
      // Invalid email format
      ToastError('Invalid email format.');
      return;
    }

    const formData: NewsletterSignupProps = {
      email,
    };

    // Handle subscription logic here
    const res = await newsletterSignup(formData);
    console.log(res);
    if (res.status === 201) {
      ToastSuccess('Welcome to our newsletter! We are excited to have you on board.');
    } else {
      ToastError('Email already exists.');
    }
  };

  const handleChange = (event: any) => {
    setEmail(event.target.value);
  };

  return (
    <div className="">
      <form onSubmit={handleSubscribe} className="mt-6 sm:flex sm:max-w-md lg:mt-0">
        <input
          type="text"
          placeholder={placeholder}
          value={email}
          onChange={handleChange}
          className="w-full min-w-0 appearance-none rounded-md border-0 bg-white px-3 py-1.5 text-base text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:w-56 sm:text-sm sm:leading-6"
        />
        <div className="mt-4 sm:ml-4 sm:mt-0 sm:flex-shrink-0">
          <Button type="submit">Subscribe</Button>
        </div>
      </form>
    </div>
  );
}
