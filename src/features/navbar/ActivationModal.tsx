import React, { Fragment, useEffect } from 'react';
import { useRouter, useSearchParams } from 'next/navigation';
import { useSelector, useDispatch } from 'react-redux';
import { Dialog, Transition } from '@headlessui/react';
import CircleLoader from 'react-spinners/CircleLoader';
import Button from '@/components/Button';
import Link from 'next/link';
import { IActivationProps } from '@/redux/actions/auth/interface';
import { activate } from '@/redux/actions/auth/actions';
import { RootState } from '@/redux/reducers';

interface ActivationModalProps {
  open: boolean;
  setOpen: (value: boolean) => void;
}

const ActivationModal: React.FC<ActivationModalProps> = ({ open, setOpen }) => {
  const dispatch = useDispatch();
  const loading = useSelector((state: RootState) => state.auth.loading);
  const activation_success = useSelector((state: RootState) => state.auth.activation_success);

  const router = useRouter();
  const searchParams = useSearchParams();
  const uid = searchParams.get('uid');
  const token = searchParams.get('token');

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    const activationData: IActivationProps = {
      uid,
      token,
    };

    dispatch(activate(activationData));
  };

  useEffect(() => {
    if (activation_success) {
      setOpen(false);
      router.push('/');
    }
  }, [activation_success]);

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as="div" className="relative z-50" onClose={setOpen}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 w-full sm:max-w-lg sm:p-6">
                <div className="sm:mx-auto sm:w-full sm:max-w-md ">
                  <p className="mb-6 pt-8 text-center text-xl font-circular-bold dark:text-dark-txt">
                    Activate your account
                  </p>
                  <div className="relative">
                    <div className="absolute inset-0 flex items-center" aria-hidden="true">
                      <div className="w-full border-t border-gray-300 dark:border-dark-second" />
                    </div>
                    <div className="relative flex justify-center" />
                  </div>
                </div>

                <div className=" sm:mx-auto sm:w-full sm:max-w-md">
                  <div className=" py-8 px-4 sm:px-10">
                    <form onSubmit={onSubmit} className="space-y-3">
                      <div>
                        {loading ? (
                          <Button className="w-full" type="button">
                            <CircleLoader loading={loading} size={25} color="#1c1d1f" />
                          </Button>
                        ) : (
                          <Button className="w-full" type="submit">
                            Activate Account
                          </Button>
                        )}
                      </div>
                    </form>
                    <p className="font-circular-light mt-2 text-xs">
                      By activating your account you accept the{' '}
                      <Link href="terms" className="text-blue-500">
                        terms of service
                      </Link>{' '}
                      and{' '}
                      <Link href="privacy" className="text-blue-500">
                        privacy policy
                      </Link>
                      .
                    </p>
                  </div>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default ActivationModal;
