import { Fragment, useEffect, useState } from 'react';
import { Bars3Icon } from '@heroicons/react/20/solid';
import { Menu, Transition } from '@headlessui/react';

import { useSearchParams } from 'next/navigation';

import Loginmodal from './LoginModal';
import Registermodal from './RegisterModal';
import ForgotPasswordModal from './ForgotPasswordModal';
import ResendActivationModal from './ResendActivationModal';
import Link from 'next/link';

import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '@/redux/reducers';

import ShoppingCart from '@/components/cart/ShoppingCart';
import ForgotPasswordConfirmModal from './ForgotPasswordConfirmModal';
import DarkModeButton from '@/components/DarkModeButton';
import ActivationModal from './ActivationModal';
import { logout, resetActivation, resetRegister } from '@/redux/actions/auth/actions';

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ');
}

interface AuthenticationProps {
  openLogin: boolean;
  setOpenLogin: React.Dispatch<React.SetStateAction<boolean>>;
}

export default function Authentication({ openLogin, setOpenLogin }: AuthenticationProps) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(resetRegister());
    dispatch(resetActivation());
  }, []);

  const searchParams = useSearchParams();

  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
  const user = useSelector((state: RootState) => state.auth.user);

  const [openRegister, setOpenRegister] = useState(false);
  const [openForgotPassword, setOpenForgotPassword] = useState(false);
  const [openForgotPasswordConfirm, setOpenForgotPasswordConfirm] = useState(false);
  const [openResendActivation, setOpenResendActivation] = useState(false);
  const [openActivation, setOpenActivation] = useState(false);

  useEffect(() => {
    if (searchParams.get('login')) {
      setOpenLogin(true);
    }
    if (searchParams.get('signup')) {
      setOpenRegister(true);
    }
    if (searchParams.get('forgot_password_confirm')) {
      setOpenForgotPasswordConfirm(true);
    }
    if (searchParams.get('activate')) {
      setOpenActivation(true);
    }
  }, [searchParams]);

  useEffect(() => {
    if (isAuthenticated) {
      setOpenLogin(false);
    }
  }, []);

  return (
    <div className="space-x-4 hidden lg:flex flex-1 items-center justify-end ">
      <ShoppingCart />
      <DarkModeButton />

      {isAuthenticated ? (
        <Menu as="div" className="relative inline-block text-left">
          <div>
            <Menu.Button className="items-center inline-flex w-full justify-center gap-x-1.5 rounded-full bg-white px-3 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-100 hover:bg-gray-50">
              <Bars3Icon className="h-5 w-auto text-gray-400" />
              <span className="inline-block h-7 w-auto overflow-hidden rounded-full bg-gray-100">
                {user ? (
                  <img className="h-full w-full object-full" src={user?.picture} alt="" />
                ) : (
                  <svg
                    className="h-full w-full text-gray-300"
                    fill="currentColor"
                    viewBox="0 0 24 24"
                  >
                    <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                  </svg>
                )}
              </span>
            </Menu.Button>
          </div>

          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            {user ? (
              <Menu.Items className="absolute right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="py-1">
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        href={`/profile`}
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'block px-4 py-2 text-sm w-full text-left font-circular-book',
                        )}
                      >
                        Profile
                      </Link>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        href={`/library`}
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'block px-4 py-2 text-sm w-full text-left font-circular-book',
                        )}
                      >
                        Library
                      </Link>
                    )}
                  </Menu.Item>

                  <Menu.Item>
                    {({ active }) => (
                      <button
                        onClick={() => {
                          dispatch(logout());
                        }}
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'block px-4 py-2 text-sm w-full text-left font-circular-book',
                        )}
                      >
                        Log out
                      </button>
                    )}
                  </Menu.Item>
                </div>
              </Menu.Items>
            ) : (
              <Menu.Items className="absolute right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="py-1">
                  <Menu.Item>
                    {({ active }) => (
                      <button
                        onClick={() => {
                          setOpenLogin(true);
                        }}
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'block px-4 py-2 text-sm w-full text-left',
                        )}
                      >
                        Login
                      </button>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <button
                        onClick={() => {
                          setOpenRegister(true);
                        }}
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'block px-4 py-2 text-sm w-full text-left',
                        )}
                      >
                        Sign up
                      </button>
                    )}
                  </Menu.Item>
                </div>
              </Menu.Items>
            )}
          </Transition>
        </Menu>
      ) : (
        <button
          type="button"
          onClick={() => {
            setOpenRegister(true);
          }}
          className="rounded-lg scale-100 hover:scale-105 transition duration-300 ease-in-out bg-blue-500 px-6 py-2 text-md font-circular-bold text-white shadow-sm hover:bg-blue-600 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-blue-600"
        >
          Start for free
        </button>
      )}

      <Loginmodal
        open={openLogin}
        setOpen={setOpenLogin}
        setOpenForgotPassword={setOpenForgotPassword}
        setOpenRegister={setOpenRegister}
      />
      <ActivationModal open={openActivation} setOpen={setOpenActivation} />

      <Registermodal
        open={openRegister}
        setOpenLogin={setOpenLogin}
        setOpen={setOpenRegister}
        setOpenResendActivation={setOpenResendActivation}
      />
      <ForgotPasswordModal
        open={openForgotPassword}
        setOpen={setOpenForgotPassword}
        setOpenLogin={setOpenLogin}
      />
      <ForgotPasswordConfirmModal
        open={openForgotPasswordConfirm}
        setOpen={setOpenForgotPasswordConfirm}
        setOpenLogin={setOpenLogin}
      />
      <ResendActivationModal
        open={openResendActivation}
        setOpen={setOpenResendActivation}
        setOpenLogin={setOpenLogin}
      />
    </div>
  );
}
