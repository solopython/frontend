import Container from './Container';
import Logo from './Logo';
import Navigation from './Navigation';

export default function Navbar() {
  return (
    <header className="dark:bg-dark-main">
      <Container>
        <Logo />
        <Navigation />
      </Container>
    </header>
  );
}
