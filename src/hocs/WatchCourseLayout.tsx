interface PageProps {
  children: React.ReactNode;
}

export default function WatchCourseLayout({ children }: PageProps) {
  return <div>{children}</div>;
}
