import Navbar from '@/components/pages/checkout/Navbar';

interface PageProps {
  children: React.ReactNode;
}

export default function CheckoutLayout({ children }: PageProps) {
  return (
    <div>
      <Navbar />
      {children}
    </div>
  );
}
