import Head from 'next/head';
import Layout from '@/hocs/Layout';
import Header from '@/components/pages/privacy/Header';
import InfoCollection from '@/components/pages/privacy/InfoCollection';
import TypesOfData from '@/components/pages/privacy/TypesOfData';
import UsageData from '@/components/pages/privacy/UsageData';
import CookiesData from '@/components/pages/privacy/CookiesData';
import UseOfData from '@/components/pages/privacy/UseOfData';
import TransferOfData from '@/components/pages/privacy/TransferOfData';
import DisclosureOfData from '@/components/pages/privacy/DisclosureOfData';
import SecurityOfData from '@/components/pages/privacy/SecurityOfData';
import ServiceProviders from '@/components/pages/privacy/ServiceProviders';
import Analytics from '@/components/pages/privacy/Analytics';
import OtherSites from '@/components/pages/privacy/OtherSites';
import ChildrensPrivacy from '@/components/pages/privacy/ChildrensPrivacy';
import Changes from '@/components/pages/privacy/Changes';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Privacy Policy`,
  description:
    'At SoloPython we are committed to protecting your privacy. Learn about our privacy policy, including how we collect, use, and disclose your personal information, and how we keep it secure.',
  href: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/privacy`,
  url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/privacy`,
  keywords: 'privacidad solopython',
  robots: 'all',
  author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export default function Page() {
  return (
    <div className="dark:bg-dark-bg">
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="isolate ">
        <Header />
        <InfoCollection />
        <TypesOfData />
        <UsageData />
        <CookiesData />
        <UseOfData />
        <TransferOfData />
        <DisclosureOfData />
        <SecurityOfData />
        <ServiceProviders />
        <Analytics />
        <OtherSites />
        <ChildrensPrivacy />
        <Changes />
      </main>
    </div>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
