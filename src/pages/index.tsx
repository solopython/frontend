import Head from 'next/head';
import Layout from '@/hocs/Layout';
import { useEffect, type ReactElement } from 'react';
import Header from '@/components/pages/home/Header';
import Stats from '@/components/pages/home/Stats';
import Feature1 from '@/components/pages/home/Feature1';
import Feature2 from '@/components/pages/home/Feature2';
import Feature3 from '@/components/pages/home/Feature3';
import EarningsCalculator from '@/components/pages/home/EarningsCalculator';
import Testimonials from '@/components/pages/home/Testimonials';
import TestimonialsLeftToRight from '@/components/pages/home/TestimonialsLeftToRight';
import Resources from '@/components/pages/home/Resources';
import CTA from '@/components/pages/home/CTA';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Aprende Programacion Python desde Cero`,
  description:
    'Cursos interactivos y de alta calidad para aprender Python. Únete a nuestra academia y transforma tu carrera en programación, sin importar tu nivel de experiencia.',
  href: '/',
  url: 'https://boomslag.com',
  keywords:
    'cursos python, aprender programacion python, curso de programacion python, programacion python desde cero',
  robots: 'all',
  author: 'SoloPython',
  publisher: 'SoloPython',
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export default function Home() {
  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div className="text-gray-700 dark:text-dark-txt space-y-6">
          <Header />
          <Stats />
          <Feature1 />
          <Feature2 />
          <Feature3 />
          <EarningsCalculator />
          <Testimonials />
          <TestimonialsLeftToRight />
          <Resources />
          <CTA />
        </div>
      </main>
    </>
  );
}

Home.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
