import Head from 'next/head';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';

import Layout from '@/hocs/Layout';
import Container from '@/components/pages/pricing/Container';
import { ITier } from '@/interfaces/tiers/Tier';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Precios y Planes`,
  description:
    'Descubre nuestros cursos de programación en Python y elige el plan que mejor se adapte a tus necesidades. Aprende a programar en Python desde cero y potencia tus habilidades de desarrollo de software con nuestros expertos instructores.',
  href: '/',
  url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/pricing`,
  keywords: '',
  robots: 'all',
  author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const res = await axios.get(`${process.env.NEXT_PUBLIC_APP_API_URL}/api/tiers/list/`);
  const tiers = res.data.results;

  return {
    props: {
      tiers,
    },
  };
}

interface PageProps {
  tiers: ITier[];
}

export default function Page({ tiers }: PageProps) {
  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div className="text-gray-700 dark:text-dark-txt space-y-6">
          <Container tiers={tiers} />
        </div>
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
