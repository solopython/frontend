import Head from 'next/head';
import Layout from '@/hocs/Layout';
import Header from '@/components/pages/terms/Header';
import InformationCollection from '@/components/pages/terms/InformationCollection';
import AccountTerms from '@/components/pages/terms/AccountTerms';
import PlanTerms from '@/components/pages/terms/PlanTerms';
import UsageTerms from '@/components/pages/terms/UsageTerms';
import PaymentTerms from '@/components/pages/terms/PaymentTerms';
import CancellationTerms from '@/components/pages/terms/CancellationTerms';
import IntellectualProperty from '@/components/pages/terms/IntellectualProperty';
import UserContent from '@/components/pages/terms/UserContent';
import ThirdParties from '@/components/pages/terms/ThirdParties';
import Indemnification from '@/components/pages/terms/Indemnification';
import Disclaimers from '@/components/pages/terms/Disclaimers';
import Limitation from '@/components/pages/terms/Limitation';
import Copyright from '@/components/pages/terms/Copyright';
import GeneralTerms from '@/components/pages/terms/GeneralTerms';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Terms of Service`,
  description:
    'Read our terms of service to learn about the legal agreements between you and Boomslag. Discover your rights and responsibilities as a user of our NFT marketplace platform.',
  href: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/terms`,
  url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/terms`,
  keywords: 'terminos y politicas solopython',
  robots: 'all',
  author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export default function Page() {
  return (
    <div className="dark:bg-dark-bg">
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="isolate ">
        <Header />
        <InformationCollection />
        <AccountTerms />
        <PlanTerms />
        <UsageTerms />
        <PaymentTerms />
        <CancellationTerms />
        <IntellectualProperty />
        <UserContent />
        <ThirdParties />
        <Indemnification />
        <Disclaimers />
        <Limitation />
        <Copyright />
        <GeneralTerms />
      </main>
    </div>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
