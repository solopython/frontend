import type { NextApiRequest, NextApiResponse } from 'next';
import cookie from 'cookie';

type Data = {
  name?: string;
  error?: string;
  success?: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === 'POST') {
    const cookies = cookie.parse(req.headers.cookie ?? '');
    const refresh = cookies.refresh ?? false;

    if (refresh === '') {
      return res.status(401).json({
        error: 'User unauthorized to make this request',
      });
    }

    const body = JSON.stringify({
      refresh,
    });

    try {
      const apiRes = await fetch(`${process.env.NEXT_PUBLIC_APP_API_URL}/auth/jwt/refresh/`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body,
      });

      const data = await apiRes.json();

      if (apiRes.status === 200) {
        res.setHeader('Set-Cookie', [
          cookie.serialize('access', data.access, {
            httpOnly: true,
            secure: process.env.NEXT_PUBLIC_APP_ENV !== 'development',
            maxAge: 900, // 15 minutes in seconds
            sameSite: 'strict',
            path: '/',
          }),
          cookie.serialize('refresh', refresh, {
            httpOnly: true,
            secure: process.env.NEXT_PUBLIC_APP_ENV !== 'development',
            maxAge: 604800, // 7 days in seconds
            sameSite: 'strict',
            path: '/',
          }),
        ]);

        return res.status(200).json({
          success: 'Refresh request successful',
        });
      } else {
        return res.status(apiRes.status).json({
          error: 'User or email already exist.',
        });
      }
    } catch (err) {
      return res.status(500).json({
        error: 'Something went wrong',
      });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    return res.status(405).json({
      error: `Method ${req.method} not allowed`,
    });
  }
}
