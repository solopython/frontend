import type { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';

type Data = {
  message?: string;
  error?: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === 'POST') {
    const { uid, token } = req.body;

    const body = JSON.stringify({
      uid,
      token,
    });

    try {
      const apiRes = await axios.post(
        `${process.env.NEXT_PUBLIC_APP_API_URL}/auth/users/activation/`,
        body,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        },
      );

      if (apiRes.status === 204) {
        res.status(204).json({ message: 'Account activated successfully!' });
      } else {
        return res.status(apiRes.status).json({
          error: 'User or email already exist.',
        });
      }
    } catch (err) {
      return res.status(500).json({
        error: 'Something went wrong',
      });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    return res.status(405).json({
      error: `Method ${req.method} not allowed`,
    });
  }
}
