import type { NextApiRequest, NextApiResponse } from 'next';

type Data = {
  error?: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === 'POST') {
    const { email } = req.body;

    const body = JSON.stringify({
      email,
    });

    try {
      const apiRes = await fetch(
        `${process.env.NEXT_PUBLIC_APP_API_URL}/auth/users/reset_password/`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body,
        },
      );

      const data = await apiRes.json();

      if (apiRes.status === 204) {
        return res.status(204).json(data);
      } else {
        return res.status(apiRes.status).json({
          error: 'There was an error.',
        });
      }
    } catch (err) {
      return res.status(500).json({
        error: 'Something went wrong',
      });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    return res.status(405).json({
      error: `Method ${req.method} not allowed`,
    });
  }
}
