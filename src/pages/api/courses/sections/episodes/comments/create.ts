import type { NextApiRequest, NextApiResponse } from 'next';
import cookie from 'cookie';

type Data = {
  name?: string;
  error?: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === 'POST') {
    const cookies = cookie.parse(req.headers.cookie ?? '');
    const access = cookies.access ?? false;

    if (!access) {
      return res.status(401).json({
        error: 'User unauthorized to make this request',
      });
    }

    const { p, page_size, max_page_size, content, episode_id } = req.body;

    const body = JSON.stringify({
      p,
      page_size,
      max_page_size,
      content,
      episode_id,
    });

    try {
      const apiRes = await fetch(
        `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/episode/comment/create/?p=${p}&page_size=${page_size}&max_page_size=${max_page_size}`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `JWT ${access}`,
          },
          body,
        },
      );

      const data = await apiRes.json();

      if (apiRes.status === 201) {
        return res.status(201).json(data);
      } else {
        return res.status(apiRes.status).json({
          error: 'Item already in cart.',
        });
      }
    } catch (err) {
      return res.status(500).json({
        error: 'Something went wrong',
      });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    return res.status(405).json({
      error: `Method ${req.method} not allowed`,
    });
  }
}
