import type { NextApiRequest, NextApiResponse } from 'next';
import cookie from 'cookie';

type Data = {
  name?: string;
  error?: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === 'POST') {
    const cookies = cookie.parse(req.headers.cookie ?? '');
    const access = cookies.access ?? false;

    if (!access) {
      return res.status(401).json({
        error: 'User unauthorized to make this request',
      });
    }

    const { p, page_size, max_page_size, comment_id } = req.body;

    const body = JSON.stringify({
      p,
      page_size,
      max_page_size,
      comment_id,
    });

    try {
      const apiRes = await fetch(
        `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/episode/comment/delete/${comment_id}/?p=${p}&page_size=${page_size}&max_page_size=${max_page_size}`,
        {
          method: 'DELETE',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `JWT ${access}`,
          },
        },
      );

      const data = await apiRes.json();

      if (apiRes.status === 200) {
        return res.status(200).json(data);
      } else {
        return res.status(apiRes.status).json({
          error: 'Error deleting comment.',
        });
      }
    } catch (err) {
      return res.status(500).json({
        error: 'Something went wrong',
      });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    return res.status(405).json({
      error: `Method ${req.method} not allowed`,
    });
  }
}
