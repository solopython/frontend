import Head from 'next/head';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import cookie from 'cookie';
import WatchCourseLayout from '@/hocs/WatchCourseLayout';
import { ICourseDetail } from '@/interfaces/courses/CourseDetail';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux/reducers';
import { Fragment } from 'react';
import Container from '@/components/pages/courses/watch/Container';

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ');
}

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { slug } = context.query;
  const cookies = cookie.parse(context.req.headers.cookie || '');
  const { access } = cookies;

  if (!access) {
    return {
      redirect: {
        destination: `/courses/view/${slug}`,
        permanent: false,
      },
    };
  }

  const courseRes = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/get/${slug}/`,
  );
  const course = courseRes.data.results;

  return {
    props: {
      course,
      slug,
    },
  };
}

interface PageProps {
  course: ICourseDetail;
  slug: string;
}

export default function Page({ course, slug }: PageProps) {
  const SeoList = {
    title: course?.title
      ? `Watch: ${course?.title} - ${course?.short_description}`
      : `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Cursos de Programacion Python`,
    description:
      course?.short_description?.slice(0, 159) ||
      'Descubre nuestros cursos de programación en Python y elige el plan que mejor se adapte a tus necesidades. Aprende a programar en Python desde cero y potencia tus habilidades de desarrollo de software con nuestros expertos instructores.',
    href: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/courses/watch/${course.slug}/`,
    url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/courses/watch/${course.slug}/`,
    keywords: course?.keywords || 'cursos de programacion python',
    robots: 'all',
    author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    image: course?.images[0].file,

    twitterHandle: '@solopython_',
  };

  return (
    <Fragment>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <Container slug={slug} course={course} />
    </Fragment>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <WatchCourseLayout>{page}</WatchCourseLayout>;
};
