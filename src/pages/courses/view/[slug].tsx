import Head from 'next/head';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import slugify from 'react-slugify';
import Layout from '@/hocs/Layout';
import { ICourseDetail } from '@/interfaces/courses/CourseDetail';
import { useRouter } from 'next/navigation';
import {
  BookOpenIcon,
  CheckBadgeIcon,
  ChevronRightIcon,
  ChevronUpIcon,
  GlobeAltIcon,
  HomeIcon,
  IdentificationIcon,
  InformationCircleIcon,
  PlayIcon,
  ReceiptRefundIcon,
  MagnifyingGlassIcon,
  StarIcon,
} from '@heroicons/react/20/solid';
import cookie from 'cookie';

import Link from 'next/link';
import sanitizeHtml from 'sanitize-html';
import Button from '@/components/Button';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '@/redux/reducers';
import { Fragment, useEffect, useState } from 'react';
import moment from 'moment';
import Clock from '@/components/timer/Clock';
import { Menu, Popover, Dialog, Disclosure, Tab, Transition } from '@headlessui/react';
import CoursePrice from '@/components/pages/courses/CoursePrice';
import CTATabs from '@/components/pages/courses/CTATabs';
import CustomVideo from '@/components/CustomVideo';
import AddToCart from '@/components/pages/courses/AddToCart';
import BuyNow from '@/components/pages/courses/BuyNow';
import CourseIncludes from '@/components/pages/courses/CourseIncludes';
import LoadingMoon from '@/components/loaders/LoadingMoon';
import WhatLearnt from '@/components/pages/courses/WhatLearnt';
import Requisites from '@/components/pages/courses/Requisites';
import CourseContentSec from '@/components/pages/courses/CourseContentSec';
import RelatedCourses from '@/components/pages/courses/RelatedCourses';
import InstructorDetails from '@/components/pages/courses/InstructorDetails';
import Reviews from '@/components/pages/courses/Reviews';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export async function getServerSideProps(context) {
  const { slug } = context.query;
  const { referrer } = context.query;

  const cookies = cookie.parse(context.req.headers.cookie || '');
  const { access } = cookies;

  let paidCoursesIdList = [];

  if (access) {
    const paidCoursesListRes = await fetch(
      `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/list_paid_by_id/`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `JWT ${access}`,
        },
      },
    );

    const data = await paidCoursesListRes.json();
    paidCoursesIdList = data.results;
  }

  const courseRes = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/get/${slug}/?referrer=${referrer}`,
  );
  const course = courseRes.data.results;

  let referrerId = referrer || '';

  return {
    props: {
      course,
      referrer: referrerId,
      paidCoursesIdList,
    },
  };
}

interface PageProps {
  course: ICourseDetail;
  referrer: string;
  paidCoursesIdList: any[];
}

export default function Page({ course, referrer, paidCoursesIdList }: PageProps) {
  const user = useSelector((state: RootState) => state.auth.user);

  const SeoList = {
    title: course?.title
      ? `${course?.title} - ${course?.short_description}`
      : `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Cursos de Programacion Python`,
    description:
      course?.short_description?.slice(0, 159) ||
      'Descubre nuestros cursos de programación en Python y elige el plan que mejor se adapte a tus necesidades. Aprende a programar en Python desde cero y potencia tus habilidades de desarrollo de software con nuestros expertos instructores.',
    href: '/',
    url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/courses/view/${course.slug}`,
    keywords: course?.keywords || 'cursos de programacion python',
    robots: 'all',
    author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    image: course?.images[0].file,

    twitterHandle: '@solopython_',
  };

  const router = useRouter();

  const allowedTags = [
    'p',
    'h1',
    'h2',
    'ul',
    'ol',
    'li',
    'sub',
    'sup',
    'blockquote',
    'pre',
    'a',
    'img',
    'video',
    'span',
    'strong',
    'em',
    'u',
    's',
    'br',
  ];

  const allowedAttributes = {
    a: ['href', 'target', 'rel'],
    img: ['src', 'alt', 'title'],
    video: ['src', 'controls'],
    span: ['style'],
    p: ['style'],
    h1: ['style'],
    h2: ['style'],
  };

  const sanitizeConfig = {
    allowedTags,
    allowedAttributes,
    allowedStyles: {
      '*': {
        'text-align': [/^left$/, /^right$/, /^center$/, /^justify$/],
        'font-size': [/^\d+(?:px|em|%)$/],
        'background-color': [/^#[0-9A-Fa-f]+$/],
        color: [/^#[0-9A-Fa-f]+$/],
        'font-family': [/^[-\w\s,"']+$/],
      },
    },
  };

  const sanitizedShortDescription = sanitizeHtml(course?.short_description, sanitizeConfig);

  const sanitizedDescription = sanitizeHtml(course?.description, sanitizeConfig);

  const shareUrl = `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/course/${course?.slug}`;
  const shareUrlAffiliate = `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/course/${course?.slug}?referrer=${user?.id}`;

  const courseExistsInPaidList = paidCoursesIdList?.some((u: any) => u.id === course?.id);

  const addToCartCTA = () => {
    return (
      <>
        <button
          type="button"
          onClick={() => {
            setOpenVideo(true);
          }}
          className="relative h-48 w-full"
        >
          <div className="absolute top-1/2 left-1/2 h-full w-full -translate-x-1/2 -translate-y-1/2 transform ">
            <div className="absolute inset-0 bg-gray-500 bg-opacity-10 " />
            <div className="absolute inset-0 bg-gradient-to-b from-transparent to-black opacity-60 transition-opacity duration-300 ease-in-out hover:opacity-20" />
          </div>
          <img
            className="h-full w-full object-cover rounded-t-lg"
            src={course.images[0].file}
            alt=""
          />
          <PlayIcon className="absolute top-1/2 left-1/2 h-16 w-16 -translate-x-1/2 -translate-y-1/2 transform  rounded-full dark:bg-dark-bg bg-white p-2.5" />
        </button>
        <div className=" p-4">
          {!courseExistsInPaidList && <CoursePrice course={course} />}
          <div className=" space-y-2">
            {courseExistsInPaidList ? (
              <button
                onClick={(e) => {
                  router.push(`/courses/watch/${course.slug}`);
                }}
                className="bg-green-600 w-full hover:bg-green-700 text-white font-bold py-3 px-4 rounded"
              >
                View Course
              </button>
            ) : (
              <>
                <AddToCart referrer={referrer} course={course} />
                <BuyNow course={course} />
              </>
            )}
            <CourseIncludes course={course} />
            <CTATabs course={course} />
          </div>
        </div>
      </>
    );
  };
  const topBar = () => {
    return (
      <div
        id="navbar"
        className="fixed top-0 z-40 hidden w-full dark:bg-dark-main bg-black bg-cover bg-center py-1.5 shadow-neubrutalism-sm "
      >
        <div>
          <div className="py-3">
            <div className="ml-5 text-lg font-bold leading-6 text-white">
              {course?.title}
              <div className="float-right flex xl:hidden">
                {/* Price */}
                <div className="flex px-4 text-white">
                  <div className="mr-4 text-base flex-shrink-0 self-end">S/{course?.price}</div>
                  <div className="mr-4 text-base flex-shrink-0 self-end">
                    {/* <Button>Buy Now</Button> */}
                  </div>
                </div>
              </div>
            </div>
            <div className="items-center">
              {
                // eslint-disable-next-line
                course?.best_seller ? (
                  <span className="mr-2 ml-4 inline-flex items-center rounded-full bg-yellow-100 px-2.5 py-0.5 text-xs font-bold text-yellow-800">
                    Best seller
                  </span>
                ) : (
                  <div />
                )
              }
              <span className={`${course?.best_seller ? '' : 'ml-4'} mr-2 inline-flex`}>
                {[0, 1, 2, 3, 4].map((rating) => (
                  <StarIcon
                    key={rating}
                    className={classNames(
                      course?.student_rating > rating ? 'text-almond-600' : 'text-gray-200',
                      'h-5 w-5 flex-shrink-0 pt-1',
                    )}
                    aria-hidden="true"
                  />
                ))}
              </span>
              <span className="text-sm text-purple-300">({course?.student_rating_no} ratings)</span>
              <span className="ml-2 text-sm font-medium text-dark hidden sm:inline-flex">
                {course?.students} Students
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  };

  function scrollFunction() {
    if (document.getElementById('navbar')) {
      if (document.body.scrollTop > 360 || document.documentElement.scrollTop > 360) {
        document.getElementById('navbar').classList.remove('hidden');
      } else {
        document.getElementById('navbar').classList.add('hidden');
      }
    }

    if (document.getElementById('navbar')) {
      if (document.body.scrollTop > 380 || document.documentElement.scrollTop > 380) {
        document.getElementById('navbar').style.top = '0';
      } else {
        document.getElementById('navbar').style.top = '-100px';
      }
    }

    if (document.getElementById('above-fold-video')) {
      if (
        document.body.scrollTop > 360 ||
        (document.documentElement.scrollTop > 360 && document.body.clientWidth > 600)
      ) {
        document.getElementById('above-fold-video').classList.add('hidden');
      } else {
        document.getElementById('above-fold-video').classList.remove('hidden');
      }
    }

    if (document.getElementById('below-fold-video')) {
      if (
        document.body.scrollTop > 360 ||
        (document.documentElement.scrollTop > 360 && document.body.clientWidth > 800)
      ) {
        document.getElementById('below-fold-video').classList.remove('hidden');
      } else {
        document.getElementById('below-fold-video').classList.add('hidden');
      }
    }
  }

  useEffect(() => {
    window.onscroll = function () {
      scrollFunction();
    };
  }, []);

  const [openVideo, setOpenVideo] = useState<boolean>(false);
  const [open, setOpen] = useState<boolean>(false);

  const whatLearntSlice: string[] = [];

  if (course && course.what_learnt && course.what_learnt.length >= 4) {
    [whatLearntSlice[0], whatLearntSlice[1], whatLearntSlice[2], whatLearntSlice[3]] =
      course.what_learnt.slice(0, 4);
  }

  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        {topBar()}
        {/* =============================================== ABOVE FOLD ============================================ */}
        <div className="relative z-0 h-auto w-full bg-cover bg-center py-32 ">
          <div className="absolute inset-0 bg-black dark:bg-dark-main bg-opacity-90" />
          <div className="mx-auto max-w-7xl px-4 sm:px-6 xl:px-8">
            <div className="mx-auto max-w-7xl">
              <div className="relative grid grid-cols-1 gap-6 lg:grid-cols-12">
                {/* DETAILS (LEFT SIDE) */}
                <div className="col-span-12 lg:col-span-7 space-y-2">
                  {/* Breadcrumbs */}
                  <nav className="flex " aria-label="Breadcrumb">
                    <ol className="flex items-center space-x-4">
                      <li>
                        <div>
                          <button
                            type="button"
                            onClick={() => {
                              router.back();
                            }}
                            className="dark:text-dark-accent dark:hover:text-dark-primary text-purple-300 hover:text-purple-400"
                          >
                            <HomeIcon className="h-5 w-5 flex-shrink-0" aria-hidden="true" />
                            <span className="sr-only">Home</span>
                          </button>
                        </div>
                      </li>
                      <li>
                        <div className="flex items-center">
                          <ChevronRightIcon
                            className="h-5 w-5 flex-shrink-0 dark:text-dark-accent text-purple-300 hover:text-purple-400"
                            aria-hidden="true"
                          />
                          <Link
                            href={`/category/${slugify(course?.category.slug)}`}
                            className="ml-4 text-sm font-bold dark:text-dark-accent dark:hover:text-dark-primary text-purple-300 hover:text-purple-400"
                          >
                            {course?.category?.name}
                          </Link>
                        </div>
                      </li>
                    </ol>
                  </nav>
                  {/* Title */}
                  <p className="text-xl font-circular-bold dark:text-dark-txt text-white  sm:tracking-tight lg:text-2xl">
                    {course?.title}
                  </p>
                  {/* Description */}
                  <div
                    className="text-md font-circular-light  text-dark-txt dark:text-dark-txt lg:text-lg"
                    dangerouslySetInnerHTML={{
                      __html: sanitizedShortDescription,
                    }}
                  />
                  {/* Ratings */}
                  <div className="">
                    {
                      // eslint-disable-next-line
                      course?.best_seller ? (
                        <span className=" mr-2 inline-flex items-center justify-center rounded-full bg-yellow-100 px-3 py-0.5 text-sm font-bold text-yellow-800">
                          Best Seller
                        </span>
                      ) : (
                        <div />
                      )
                    }
                    <div className="mr-2  inline-flex items-center">
                      {[0, 1, 2, 3, 4].map((rating) => (
                        <StarIcon
                          key={rating}
                          className={classNames(
                            course?.student_rating > rating ? 'text-yellow-600' : 'text-gray-200',
                            'h-5 w-5 flex-shrink-0',
                          )}
                          aria-hidden="true"
                        />
                      ))}
                    </div>
                    <span className="text-md font-regular dark:text-dark-accent text-purple-300">
                      ({course?.student_rating_no} ratings){' '}
                      <span className="text-md ml-2 font-medium text-white">
                        {course?.students} Students
                      </span>
                    </span>
                  </div>

                  {/* Author */}
                  <div className="flex">
                    <div>
                      <p className=" text-sm font-medium text-white ">
                        Created by:{' '}
                        <span className="font-bold dark:text-dark-accent dark:hover:text-dark-primary text-iris-400 underline">
                          <Link href={`/@/${course?.author.username}`}>
                            {course?.author.username}
                          </Link>
                          {course?.author.verified ? (
                            <CheckBadgeIcon
                              className="ml-1 inline-flex h-4 w-4 dark:text-dark-accent  text-iris-400"
                              aria-hidden="true"
                            />
                          ) : (
                            <div />
                          )}
                        </span>
                      </p>

                      <span className="mt-1 block" />
                    </div>
                  </div>

                  {/* Details */}
                  <div className="mt-1 flex gap-x-4">
                    <div>
                      <InformationCircleIcon className="mr-2 inline-flex h-5 w-5 text-white dark:text-dark-txt " />
                      <span className="text-xs font-medium text-white dark:text-dark-txt sm:text-sm">
                        <span>Updated </span>
                        {moment(course?.updated_at).format('MMMM YYYY')}
                      </span>
                    </div>
                    <div>
                      <GlobeAltIcon className="mr-2 inline-flex h-5 w-5 text-white dark:text-dark-txt" />
                      <span className="font-regular text-xs text-white dark:text-dark-txt sm:text-sm">
                        {course?.language}
                      </span>
                    </div>
                  </div>
                </div>
                {/* VIDEO (RIGHT SIDE) */}
                <div className="absolute right-0 col-span-5 hidden items-start lg:flex">
                  <div
                    id="above-fold-video"
                    className="w-[397px]  dark:bg-dark-second dark:border-dark-border rounded-lg bg-white dark:shadow-none shadow-lg  lg:row-span-3"
                  >
                    {addToCartCTA()}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* =============================================== BELOW FOLD ============================================ */}
        <main className=" pb-8">
          <div className="mx-auto max-w-4xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
            <h1 className="sr-only">Page title</h1>
            {/* Main 3 column grid */}
            <div className="grid grid-cols-1 items-start gap-4 lg:grid-cols-3 lg:gap-8">
              {/* Left column */}
              <div className="grid grid-cols-1 gap-4 lg:col-span-2">
                <section aria-labelledby="section-1-title">
                  <h2 className="sr-only" id="section-1-title">
                    Section title
                  </h2>
                  <div className="overflow-hidden">
                    <div className="p-6">
                      <p className="py-4 text-2xl font-black leading-6 text-gray-900 dark:text-dark-txt">
                        Course overview
                      </p>
                      <ul className="">
                        {whatLearntSlice &&
                          whatLearntSlice.map((item) => (
                            <li key={item.id} className="space-y-1">
                              <span className="text-md font-medium dark:text-dark-txt-secondary">
                                &#9679; {item.title}
                              </span>
                            </li>
                          ))}
                      </ul>

                      <div className="mt-4 flex-auto md:space-x-1 py-2 md:grid md:grid-cols-3 ">
                        <div className="border border-gray-400 p-4 dark:border-dark-third">
                          <dt>
                            <div className="flex h-8 w-8 items-center justify-center rounded-full dark:bg-dark-txt bg-gray-700 text-white dark:text-dark-third">
                              <PlayIcon className="h-5 ml-0.5 w-auto" aria-hidden="true" />
                            </div>
                            <p className="mt-12 text-lg font-medium leading-6 text-gray-700" />
                          </dt>
                          <dd className="mt-2 text-xs font-black text-gray-900 dark:text-dark-txt-secondary">
                            {course?.total_duration} hours of video
                          </dd>
                        </div>

                        <div className="border border-gray-400 p-4 dark:border-dark-third">
                          <dt>
                            <div className="flex h-8 w-8 items-center justify-center text-gray-700  dark:text-dark-txt">
                              <BookOpenIcon className="h-8 w-8" aria-hidden="true" />
                            </div>
                            <p className="mt-12 text-lg font-medium leading-6 text-gray-700" />
                          </dt>
                          <dd className="mt-2 text-xs font-black text-gray-900 dark:text-dark-txt-secondary">
                            {course?.total_lectures} articles + resources
                          </dd>
                        </div>

                        {/* <div className="border border-gray-300 p-4">
                                            <dt>
                                            <div className="flex h-8 w-8 items-center justify-center text-gray-700">
                                                <CommandLineIcon className="h-8 w-8" aria-hidden="true" />
                                            </div>
                                            <p className="mt-12 text-lg font-medium leading-6 text-gray-700"></p>
                                            </dt>
                                            <dd className="mt-2 text-xs text-gray-900 font-bold">{sections.length} coding exercises</dd>
                                        </div> */}

                        <div className="border border-gray-400 p-4 dark:border-dark-third">
                          <dt>
                            <div className="flex h-8 w-8 items-center justify-center text-gray-900  dark:text-dark-txt ">
                              <IdentificationIcon className="h-8 w-8" aria-hidden="true" />
                            </div>
                            <p className="mt-12 text-lg font-medium leading-6 text-gray-900" />
                          </dt>
                          <dd className="mt-2 text-xs font-black text-gray-900 dark:text-dark-txt-secondary">
                            certificate of completion
                          </dd>
                        </div>
                      </div>

                      <div className="mt-2 ">
                        <button
                          className=" 
                            text-md focus:ring-none inline-flex 
                            w-full
                            items-center
                            justify-center 
                            border
                            border-dark-bg 
                            bg-white dark:bg-dark-primary dark:text-white
                            px-4 
                            py-3 
                            text-sm 
                            font-bold
                            text-dark
                            shadow-neubrutalism-sm transition
                            duration-300
                            ease-in-out  hover:-translate-x-0.5  hover:-translate-y-0.5  hover:bg-gray-50 hover:shadow-neubrutalism-md "
                          onClick={() => setOpen(true)}
                          type="button"
                        >
                          Show full overview
                        </button>
                      </div>
                      <Transition.Root show={open} as={Fragment}>
                        <Dialog as="div" className="relative z-50" onClose={setOpen}>
                          <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                          >
                            <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                          </Transition.Child>

                          <div className="fixed inset-0 z-40 overflow-y-auto">
                            <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                              <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                                enterTo="opacity-100 translate-y-0 sm:scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                                leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                              >
                                <Dialog.Panel className="relative w-full transform overflow-hidden rounded-lg bg-white px-4 pt-5 pb-4 text-left shadow-xl transition-all dark:bg-dark-main sm:my-8 sm:max-w-3xl sm:p-6">
                                  <div className=" pb-5">
                                    <h3 className="text-lg font-bold leading-6 text-gray-900 dark:text-dark-txt">
                                      Details and curriculum
                                    </h3>
                                  </div>
                                  <WhatLearnt whatlearnt={course && course.what_learnt} />

                                  {/* Description */}
                                  <Disclosure>
                                    {({ open }) => (
                                      <>
                                        <Disclosure.Button className=" flex w-full justify-between border-y border-gray-300 px-4 py-2  text-left text-2xl font-black text-gray-900 dark:border-dark-third dark:text-dark-txt">
                                          <span>Description</span>
                                          <ChevronUpIcon
                                            className={`${
                                              open ? 'rotate-180 transform' : ''
                                            } mt-1 h-5 w-5 text-gray-500`}
                                          />
                                        </Disclosure.Button>
                                        <Disclosure.Panel className="px-4 py-2 text-sm text-gray-500">
                                          <div
                                            className="text-md font-regular my-2 text-gray-900 dark:text-dark-txt"
                                            dangerouslySetInnerHTML={{
                                              __html: sanitizedDescription,
                                            }}
                                          />
                                        </Disclosure.Panel>
                                      </>
                                    )}
                                  </Disclosure>
                                  {/* Requirements */}
                                  <Disclosure>
                                    {({ open }) => (
                                      <>
                                        <Disclosure.Button className="mt-4 flex w-full justify-between border-y border-gray-300 px-4 py-2  text-left text-2xl font-black text-gray-900 dark:border-dark-third dark:text-dark-txt">
                                          <span>Requirements</span>
                                          <ChevronUpIcon
                                            className={`${
                                              open ? 'rotate-180 transform' : ''
                                            } mt-1 h-5 w-5 text-gray-500`}
                                          />
                                        </Disclosure.Button>
                                        <Disclosure.Panel className="px-4 py-2 text-sm text-gray-500">
                                          <Requisites requisites={course && course.requisites} />
                                        </Disclosure.Panel>
                                      </>
                                    )}
                                  </Disclosure>
                                </Dialog.Panel>
                              </Transition.Child>
                            </div>
                          </div>
                        </Dialog>
                      </Transition.Root>

                      {/* SoloPython for Business */}
                      <div className="mt-4">
                        <div className="mx-auto max-w-7xl border border-gray-300 p-4 dark:border-dark-third">
                          <h2 className="text-md font-bold tracking-tight text-gray-900 dark:text-dark-txt">
                            Access to More Courses for Less
                          </h2>

                          <div className=" pb-2 sm:flex sm:items-center sm:justify-between">
                            <p className="font-regular text-sm tracking-tight text-gray-900 dark:text-dark-txt">
                              You may access this course if you subscribe to any of these.
                            </p>
                            <div className="mt-3 sm:mt-0 sm:ml-4">
                              <Link
                                href={`/pricing`}
                                className=" inline-flex items-center rounded-md border border-transparent px-4 py-2 text-sm font-medium dark:text-dark-accent hover:dark:text-dark-primary text-purple-700 underline underline-offset-4 "
                              >
                                Subscribe
                              </Link>
                            </div>
                          </div>

                          <div className="mt-4 flow-root ">
                            <div className="-mt-4 -ml-8 flex flex-wrap justify-between lg:-ml-4">
                              <div className="mt-4 ml-8 flex flex-shrink-0 flex-grow lg:ml-4 lg:flex-grow-0">
                                <span className="bg-green-100 text-green-800 inline-flex items-center rounded-md px-2.5 py-0.5 text-sm font-medium">
                                  Tier name
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* Sections */}
                      <div className="mt-4">
                        <div className="border-b border-gray-200 pb-5">
                          <h2 className="mb-2 text-2xl font-black text-gray-800 dark:text-dark-txt md:text-3xl">
                            Course Content
                          </h2>

                          <ul className="flex w-full text-xs font-medium dark:text-dark-txt-secondary md:text-sm">
                            <li className="mr-1 inline-block ">
                              {course?.sections.length} sections
                            </li>
                            <li className="mr-1  inline-block">
                              • {course?.total_lectures} lectures
                            </li>
                            <li className="mr-1  inline-block">
                              • {course?.total_duration} h total length
                            </li>
                          </ul>
                        </div>
                        <div className="my-4 dark:text-dark-txt">
                          {
                            // eslint-disable-next-line
                            course?.sections ? (
                              course.sections.map((section) => (
                                <CourseContentSec section={section} key={section.id} />
                              ))
                            ) : (
                              <div />
                            )
                          }
                        </div>
                      </div>
                      {/* Related */}

                      <div className="">
                        <RelatedCourses courses={[]} />
                      </div>

                      <div className="">
                        <InstructorDetails author={course?.author} />
                      </div>

                      {/* Reviews */}
                      <div className="">
                        <Reviews course={course} />
                      </div>
                      {/* Hre ends details left */}
                    </div>
                  </div>
                </section>
              </div>

              {/* Right column */}
              <div
                id="below-fold-video"
                className="lg:sticky hidden top-14 lg:z-50 grid grid-cols-1"
              >
                <section aria-labelledby="section-2-title">
                  <h2 className="sr-only" id="section-2-title">
                    Section title
                  </h2>
                  <div className="overflow-hidden rounded-lg -ml-3.5 bg-white shadow-lg">
                    <div className="">{addToCartCTA()}</div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </main>

        {/* Video Play */}
        <Transition.Root show={openVideo} as={Fragment}>
          <Dialog as="div" className="relative z-20" onClose={setOpenVideo}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
            </Transition.Child>

            <div className="fixed inset-0 z-10 overflow-y-auto">
              <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                >
                  <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-black px-4 pt-5 pb-4 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-2xl sm:p-6">
                    <div>
                      <div className=" px-4 py-5 sm:px-6">
                        <div className="-ml-4 -mt-4 flex flex-wrap items-center justify-between sm:flex-nowrap">
                          <div className="ml-4 mt-4">
                            <h3 className="text-sm font-medium leading-6 text-gray-500">
                              Course Preview
                            </h3>
                            <p className="text-md  mt-1 font-bold text-white">{course?.title}</p>
                          </div>
                          <div className="ml-4 mt-4 flex-shrink-0">
                            {/* <button
                                        type="button"
                                        className="relative inline-flex items-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                    >
                                        Create new job
                                    </button> */}
                          </div>
                        </div>
                      </div>
                      <div className="px-4 py-2 text-white sm:px-4">
                        <video
                          src={course && course.videos[0].file}
                          controls
                          poster={course?.images[0].file}
                          className="w-full h-96"
                        />
                        <div className="h-full w-full bg-white" />
                      </div>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition.Root>
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
