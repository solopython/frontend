import Head from 'next/head';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';

import Layout from '@/hocs/Layout';
import { ITier } from '@/interfaces/tiers/Tier';
import { ICategory } from '@/interfaces/category/Category';
import { ICourseList } from '@/interfaces/courses/CoursesList';
import CategoriesList from '@/components/categories/CategoriesList';
import CourseSearch from '@/components/pages/courses/CourseSearch';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} Courses - Explore Premium Online Courses`,
  description:
    'Find and buy high-quality online courses at SoloPython, a cutting-edge marketplace where you can acquire knowledge through secure and seamless transactions.',
  href: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/courses`,
  url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/courses`,
  keywords:
    'cursos de programacion python, cursos python, cursos de programacion python para principiantes, cursos avanzados de programacion python, cursos de programacion python para ciencia de datos, cursos de programacion python en peru, mejores cursos online de programacion python para principiantes',
  robots: 'all',
  author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const categoriesrRes = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/category/list/parent/`,
  );
  const categories = categoriesrRes.data.results;

  const coursesRes = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/list/?p=1&page_size=12&max_page_size=100`,
  );
  const courses = coursesRes.data.results;

  return {
    props: {
      categories,
      courses,
    },
  };
}

interface PageProps {
  categories: ICategory[];
  courses: ICourseList[];
}

export default function Page({ categories, courses }: PageProps) {
  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div className="text-gray-700 dark:text-dark-txt space-y-6">
          <CategoriesList categories={categories} />
          <CourseSearch categories={categories} courses={courses} />
        </div>
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
