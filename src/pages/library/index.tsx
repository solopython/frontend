import Head from 'next/head';
import Layout from '@/hocs/Layout';
import { GetServerSidePropsContext } from 'next';
import cookie from 'cookie';
import { RootState } from '@/redux/reducers';
import { useSelector } from 'react-redux';
import Header from '@/components/library/Header';
import { useCallback, useEffect, useState } from 'react';
import { ICourseList } from '@/interfaces/courses/CoursesList';
import { FetchPaidCoursesProps, fetchPaidCourses } from '@/utils/api/listPaidCourses';
import CoursesList from '@/components/library/CoursesList';
import { useDebounce } from '@/hooks/debounceHook';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Libreria de Cursos`,
  description:
    'Cursos interactivos y de alta calidad para aprender Python. Únete a nuestra academia y transforma tu carrera en programación, sin importar tu nivel de experiencia.',
  href: '/',
  url: 'https://boomslag.com',
  keywords:
    'cursos python, aprender programacion python, curso de programacion python, programacion python desde cero',
  robots: 'all',
  author: 'SoloPython',
  publisher: 'SoloPython',
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const cookies = cookie.parse(context.req.headers.cookie || '');
  const { access } = cookies;

  if (!access) {
    return {
      redirect: {
        destination: `/`,
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
}

export default function Page() {
  const [searchBy, setSearchBy] = useState<string>('');
  const debouncedSearchBy = useDebounce({ value: searchBy, delay: 1000 }); // Debounced value of searchBy

  const [courses, setCourses] = useState<ICourseList[]>([]);
  const [count, setCount] = useState<number>(0);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [pageSize, setCurrentPageSize] = useState<number>(12);
  const [maxPageSize, setMaxPageSize] = useState<number>(100);
  const [sortBy, setSortBy] = useState<string>('');
  const [orderBy, setOrderBy] = useState<string>('');
  const [category, setCategory] = useState<string>('');
  const [author, setAuthor] = useState<string>('');
  const [levels, setLevels] = useState<string>('');

  const fetchCourses = useCallback(
    async (pageSize: number, searchBy: string) => {
      const data: FetchPaidCoursesProps = {
        p: currentPage,
        page_size: pageSize,
        max_page_size: maxPageSize,
        sorting: sortBy,
        order: orderBy,
        category: category,
        author: author,
        search: searchBy,
        levels: levels,
      };

      const res = await fetchPaidCourses(data);
      setCourses(res.results);
      setCount(res.count);
    },
    [currentPage, maxPageSize, sortBy, orderBy, category, author, levels],
  );

  useEffect(() => {
    fetchCourses(pageSize, debouncedSearchBy);
  }, [fetchCourses, pageSize, debouncedSearchBy]);

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    fetchCourses(pageSize, debouncedSearchBy);
  };

  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div className="text-gray-700 dark:text-dark-txt space-y-6">
          <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
            {/* We've used 3xl here, but feel free to try other max-widths based on your needs */}
            <div className="mx-auto max-w-7xl py-32">
              {/* Content goes here */}
              <Header
                sortBy={sortBy}
                setSortBy={setSortBy}
                orderBy={orderBy}
                setOrderBy={setOrderBy}
                searchBy={searchBy}
                setSearchBy={setSearchBy}
                setCategory={setCategory}
                setAuthor={setAuthor}
                setLevels={setLevels}
                onSubmit={onSubmit}
              />
              <CoursesList
                courses={courses}
                count={count}
                pageSize={pageSize}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
              />
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
