import '@/styles/globals.css';
import '@/styles/customVideo.css';
import '@/styles/checkout.css';
import '@/styles/fonts.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import type { AppProps } from 'next/app';
import type { NextComponentType } from 'next';
import { NextPageContext } from 'next/types';
import { Provider } from 'react-redux';
import wrapper from '@/redux/store';

import { ThemeProvider } from 'next-themes';

import '@/styles/toastStyles.css';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { useEffect } from 'react';
import ContextProviders from '@/components/providers/ContextProviders';

NProgress.configure({ showSpinner: false });

export type NextLayoutComponentType<P = {}> = NextComponentType<NextPageContext, any, P> & {
  getLayout?: (page: React.ReactNode) => React.ReactNode;
};

export default function App({ Component, pageProps }: AppProps<NextLayoutComponentType>) {
  useEffect(() => {
    const handleRouteChangeStart = () => {
      NProgress.start();
    };

    const handleRouteChangeComplete = () => {
      NProgress.done();
    };

    const handleRouteChangeError = () => {
      NProgress.done();
    };

    Router.events.on('routeChangeStart', handleRouteChangeStart);
    Router.events.on('routeChangeComplete', handleRouteChangeComplete);
    Router.events.on('routeChangeError', handleRouteChangeError);

    return () => {
      Router.events.off('routeChangeStart', handleRouteChangeStart);
      Router.events.off('routeChangeComplete', handleRouteChangeComplete);
      Router.events.off('routeChangeError', handleRouteChangeError);
    };
  }, []);

  const getLayout =
    (Component as NextLayoutComponentType).getLayout || ((page: React.ReactNode) => page);

  const { store, props } = wrapper.useWrappedStore(pageProps);
  return (
    <Provider store={store}>
      <ContextProviders>
        <ThemeProvider enableSystem attribute="class">
          {getLayout(<Component {...props} />)}
          <ToastContainer className="bottom-0" position="bottom-right" />
        </ThemeProvider>
      </ContextProviders>
    </Provider>
  );
}
