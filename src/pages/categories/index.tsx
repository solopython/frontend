import Head from 'next/head';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';

import Layout from '@/hocs/Layout';
import Link from 'next/link';
import { ICategory } from '@/interfaces/category/Category';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Precios y Planes`,
  description:
    'Descubre nuestros cursos de programación en Python y elige el plan que mejor se adapte a tus necesidades. Aprende a programar en Python desde cero y potencia tus habilidades de desarrollo de software con nuestros expertos instructores.',
  href: '/',
  url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/pricing`,
  keywords: '',
  robots: 'all',
  author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const res = await axios.get(`${process.env.NEXT_PUBLIC_APP_API_URL}/api/category/list/popular/`);
  const categories = res.data.results;

  return {
    props: {
      categories,
    },
  };
}

interface PageProps {
  categories: ICategory[];
}

export default function Page({ categories }: PageProps) {
  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div className="text-gray-700 dark:text-dark-txt space-y-6">
          <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
            {/* We've used 3xl here, but feel free to try other max-widths based on your needs */}
            <div className="mx-auto max-w-7xl ">
              {/* Content goes here */}
              <div className="">
                <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                  <div className="mx-auto max-w-2xl py-16 sm:py-24 lg:max-w-none lg:py-32">
                    <h2 className="text-2xl font-bold text-gray-900">Collections</h2>

                    <div className="mt-6 space-y-12 lg:grid lg:grid-cols-3 lg:gap-x-6 lg:space-y-0">
                      {categories.map((category) => (
                        <div key={category.slug} className="group relative">
                          <div className="relative h-80 w-full overflow-hidden rounded-lg bg-white sm:aspect-h-1 sm:aspect-w-2 lg:aspect-h-1 lg:aspect-w-1 group-hover:opacity-75 sm:h-64">
                            <img
                              src={category.thumbnail}
                              alt=""
                              className="h-full w-full object-cover object-center"
                            />
                          </div>
                          <h3 className="mt-6 text-sm text-gray-500">
                            <Link href={`/categories/view/${category.slug}`}>
                              <span className="absolute inset-0" />
                              {category.name}
                            </Link>
                          </h3>
                          <p className="text-base font-semibold text-gray-900">
                            {category.description}
                          </p>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
