import Head from 'next/head';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { ICourseList } from '@/interfaces/courses/CoursesList';
import Layout from '@/hocs/Layout';
import { ICategory } from '@/interfaces/category/Category';
import CoursesList from '@/components/pages/courses/CoursesList';

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ');
}

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { slug } = context.query;

  const courseRes = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/list/?category=${slug}`,
  );
  const courses = courseRes.data.results;

  const categoryRes = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/category/get/?category=${slug}`,
  );
  const category = categoryRes.data.results;

  return {
    props: {
      courses,
      slug,
      category,
    },
  };
}

interface PageProps {
  courses: ICourseList[];
  slug: string;
  category: ICategory;
}

export default function Page({ courses, slug, category }: PageProps) {
  const capitalizeFirstLetter = (string: string) =>
    string.charAt(0).toUpperCase() + string.slice(1);

  const capitalizedSlug = capitalizeFirstLetter(slug);

  const SeoList = {
    title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Cursos de ${slug}`,
    description: category?.description || 'Descubre nuestros cursos de programación en Python.',
    href: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/categories/${slug}/`,
    url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/categories/${slug}/`,
    keywords: category?.keywords || 'cursos de programacion python',
    robots: 'all',
    author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    image: category?.thumbnail,

    twitterHandle: '@solopython_',
  };

  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div>
          <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
            {/* We've used 3xl here, but feel free to try other max-widths based on your needs */}
            <div className="mx-auto max-w-7xl py-12">
              {/* Content goes here */}
              <div className="pt-28">
                <h3 className="text-3xl font-bold leading-6 dark:text-dark-txt text-gray-900">
                  Cursos de {capitalizedSlug}
                </h3>
              </div>
              <CoursesList courses={courses} />
            </div>
          </div>
          {/* <CourseSearch categories={categories} courses={courses} /> */}
        </div>
      </main>
    </>
  );
}
Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
