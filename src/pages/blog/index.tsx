import Head from 'next/head';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';

import Layout from '@/hocs/Layout';
import { ICategory } from '@/interfaces/category/Category';
import { IPost } from '@/interfaces/blog/Post';
import Container from '@/components/pages/blog/Container';
import Header from '@/components/pages/blog/Header';
import Featured from '@/components/pages/blog/Featured';
import FeaturedMore from '@/components/pages/blog/FeaturedMore';
import Recommended from '@/components/pages/blog/Recommended';
import EmailCTA from '@/components/pages/blog/EmailCTA';
import BlogSearch from '@/components/pages/blog/BlogSearch';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Precios y Planes`,
  description:
    'Descubre nuestros cursos de programación en Python y elige el plan que mejor se adapte a tus necesidades. Aprende a programar en Python desde cero y potencia tus habilidades de desarrollo de software con nuestros expertos instructores.',
  href: '/',
  url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/pricing`,
  keywords: '',
  robots: 'all',
  author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const categoriesrRes = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/category/list/parent/`,
  );
  const categories = categoriesrRes.data.results;

  const postsRes = await axios.get(`${process.env.NEXT_PUBLIC_APP_API_URL}/api/blog/posts/list/`);
  const posts = postsRes.data.results;

  return {
    props: {
      categories,
      posts,
    },
  };
}

interface PageProps {
  categories: ICategory[];
  posts: IPost[];
}

export default function Page({ categories, posts }: PageProps) {
  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div className="text-gray-700 dark:text-dark-txt space-y-6">
          <Container>
            <Header />
            <Featured posts={posts} />
            <div className="mx-auto max-w-7xl text-center">
              <div className="w-full border-b border-gray-200 my-12" />
            </div>
            <FeaturedMore posts={posts} />
            <Recommended posts={posts} />
            <EmailCTA />
            <BlogSearch posts={posts} categories={categories} />
          </Container>
        </div>
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
