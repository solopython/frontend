import { GetServerSidePropsContext } from 'next';
import axios from 'axios';
import Head from 'next/head';
import DOMPurify from 'isomorphic-dompurify';
import Layout from '@/hocs/Layout';
import Link from 'next/link';
import { IPost } from '@/interfaces/blog/Post';
import { EnvelopeIcon } from '@heroicons/react/20/solid';
import EmailForm from '@/components/pages/blog/EmailForm';
import ReturnToPosts from '@/components/pages/blog/ReturnToPosts';
import { IHeading } from '@/interfaces/blog/Heading';

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { slug } = context.query;

  const res = await axios.get(
    `${process.env.NEXT_PUBLIC_APP_API_URL}/api/blog/posts/get/?slug=${slug}`,
  );
  const post = res.data.results.post;
  const headings = res.data.results.headings;
  return {
    props: {
      post,
      headings,
    },
  };
}

interface PageProps {
  post: IPost;
  headings: IHeading[];
}

export default function Page({ post, headings }: PageProps) {
  const SeoList = {
    title: post?.title
      ? `${post?.title} - ${post?.description}`
      : `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Blog Post`,
    description:
      post?.description?.slice(0, 159) ||
      'Descubre nuestros cursos de programación en Python y elige el plan que mejor se adapte a tus necesidades. Aprende a programar en Python desde cero y potencia tus habilidades de desarrollo de software con nuestros expertos instructores.',
    href: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/blog/post/${post.slug}`,
    url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/blog/post/${post.slug}`,
    keywords: post?.keywords || 'cursos de programacion python',
    robots: 'all',
    author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
    image: post?.thumbnail,

    twitterHandle: '@solopython_',
  };

  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <div className="bg-white px-6 py-32 lg:px-8">
        <div className="mx-auto border-b border-gray-200 max-w-7xl px-6 lg:px-8 py-10">
          <div className="mx-auto max-w-4xl lg:max-w-7xl">
            <div className="grid grid-cols-1 lg:grid-cols-12">
              <div className="lg:col-span-7  m-2 p-8">
                <ReturnToPosts />
                <h3 className="text-2xl my-6 lg:text-4xl font-circular-bold ">{post?.title}</h3>
                <Link
                  href={`/categories/${post?.category?.slug}`}
                  className="mt-4 text-md font-circular-medium text-blue-500 text-md "
                >
                  Category: {post?.category?.name}
                </Link>
              </div>
              <div className="col-span-5 m-2 rounded p-6">
                <img src={post?.thumbnail} className="object-cover w-auto h-full" />
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-wrap relative">
          <div className="w-1/4 sticky top-32 h-full">
            <div className="col-span-5 bg-light-gray m-2 rounded p-6">
              <h1 className="text-xl font-circular-medium mb-2">Table of contents</h1>
              <ul className="space-y-2">
                {headings?.map((heading, idx) => (
                  <div key={idx}>
                    <li className="cursor-pointer hover:text-blue-500 font-circular-medium text-gray-500">
                      {heading.title}
                    </li>
                    <div className="w-full border-b border-gray-300 my-2" />
                  </div>
                ))}
              </ul>
            </div>
          </div>

          <div className="w-1/2 px-2">
            <div className="mx-auto max-w-3xl text-base leading-7 text-gray-700">
              <p className="mt-6 text-xl font-circular-book leading-8">{post?.description}</p>
              <h1 className="mt-8 text-3xl font-circular-bold tracking-tight text-blue-500 sm:text-4xl">
                {post?.title}
              </h1>
              <div className="mt-10 max-w-2xl">
                <div
                  className="font-circular-light text-lg prose"
                  dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(post?.content) }}
                />
              </div>
            </div>
          </div>

          <div className="w-1/4 sticky top-20 h-full">
            <div className="col-span-5 bg-blue-500 m-2 rounded p-6">
              <EnvelopeIcon className="h-6 w-auto text-white mb-4" />
              <h3 className="text-2xl font-semibold leading-6 text-white">Stay in the loop</h3>
              <p className="mt-4 text-md font-circular-light text-lg text-gray-100">
                Get free expert insights and tips to grow your knowledge business sent right to your
                inbox.{' '}
              </p>
              <EmailForm />
              <p className="text-xs font-circular-light text-white leading-normal mt-2">
                By submitting you agree to receive our monthly Knowledge Economy Newsletter as well
                as other promotional emails from Kajabi. You may withdraw your consent at any time
                via the “Unsubscribe” link in any email or view our privacy policy at any time.
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>;
};
