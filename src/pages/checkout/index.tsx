import Head from 'next/head';
import cookie from 'cookie';
import { GetServerSidePropsContext } from 'next';
import CheckoutLayout from '@/hocs/CheckoutLayout';
import ConfettiContainer from '@/components/pages/checkout/ConfettiContainer';
import PaymentMethod from '@/components/pages/checkout/PaymentMethod';
import OrderItems from '@/components/pages/checkout/OrderItems';
import OrderSummary from '@/components/pages/checkout/OrderSummary';
import CreditCardPayment from '@/components/pages/checkout/CreditCardPayment';

const SeoList = {
  title: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME} - Checkout`,
  description:
    'Completa tu compra en Solopython, la plataforma definitiva de NFT para cursos en línea, productos físicos y más. Experimenta el futuro del comercio electrónico con el poder de la tecnología blockchain. Descubre nuestra academia que ofrece cursos en línea sobre programación en Python y lleva tus habilidades al siguiente nivel.',
  href: '/',
  url: `${process.env.NEXT_PUBLIC_APP_PUBLIC_URL}/checkout`,
  keywords: '',
  robots: 'all',
  author: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  publisher: `${process.env.NEXT_PUBLIC_APP_DOMAIN_NAME}`,
  image: 'https://solopython.s3.sa-east-1.amazonaws.com/background.jpg',

  twitterHandle: '@solopython_',
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const cookies = cookie.parse(context.req.headers.cookie || '');
  const { access } = cookies;

  if (!access) {
    return {
      redirect: {
        destination: `/`,
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
}

interface PageProps {}

export default function Page({}: PageProps) {
  return (
    <>
      <Head>
        <title>{SeoList.title}</title>
        <meta name="description" content={SeoList.description} />

        <meta name="keywords" content={SeoList.keywords} />
        <link rel="canonical" href={SeoList.href} />
        <meta name="robots" content={SeoList.robots} />
        <meta name="author" content={SeoList.author} />
        <meta name="publisher" content={SeoList.publisher} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />

        {/* Social Media Tags */}
        <meta property="og:title" content={SeoList.title} />
        <meta property="og:description" content={SeoList.description} />
        <meta property="og:url" content={SeoList.url} />
        <meta property="og:image" content={SeoList.image} />
        <meta property="og:image:width" content="1370" />
        <meta property="og:image:height" content="849" />
        <meta property="og:image:alt" content={SeoList.image} />
        <meta property="og:type" content="website" />

        <meta property="fb:app_id" content="555171873348164" />

        {/* Twitter meta Tags */}
        <meta name="twitter:title" content={SeoList.title} />
        <meta name="twitter:description" content={SeoList.description} />
        <meta name="twitter:image" content={SeoList.image} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={SeoList.twitterHandle} />
        <meta name="twitter:player:width" content="1280" />
        <meta name="twitter:player:height" content="720" />
      </Head>
      <main className="dark:bg-dark-main">
        <div className="text-gray-700 dark:text-dark-txt">
          <ConfettiContainer>
            <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
              {/* We've used 3xl here, but feel free to try other max-widths based on your needs */}
              <div className="mx-auto max-w-7xl">
                {/* Content goes here */}
                <div className="grid-cols-0 grid  md:grid-cols-2">
                  <div className="col-span-1 dark:bg-dark-bg bg-white">
                    <div className=" py-4 sm:flex ">
                      <div className="font-recife-bold text-2xl leading-6" />
                    </div>

                    <div className=" pt-6  sm:flex ">
                      <h3 className="font-recife-bold text-4xl leading-6 dark:text-dark-txt text-gray-900">
                        Checkout
                      </h3>
                    </div>

                    <div className=" pt-12 pb-6 sm:flex ">
                      <h3 className="text-2xl font-black leading-6 dark:text-dark-txt text-gray-900">
                        Payment Method
                      </h3>
                    </div>

                    <div className="pr-8">
                      <PaymentMethod />
                    </div>

                    <div className=" pt-14 pb-6 sm:flex ">
                      <h3 className="text-2xl font-black leading-6 dark:text-dark-txt text-gray-900">
                        Order Items
                      </h3>
                    </div>
                    <div className="pr-8 pb-8">
                      <OrderItems />
                    </div>
                  </div>
                  <div className="col-span-1 items-center justify-center dark:bg-dark-second bg-gray-50 text-center">
                    <div className="sticky top-0 mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                      <div className=" mt-0 py-3 sm:flex ">
                        <div className="font-recife-bold text-2xl leading-6 " />
                      </div>
                      <div className=" py-6 sm:flex">
                        <h3 className="text-2xl font-black leading-6 dark:text-dark-txt text-gray-900">
                          Summary
                        </h3>
                      </div>
                      <OrderSummary />
                      <CreditCardPayment />
                      <div className="pb-8" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </ConfettiContainer>
        </div>
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <CheckoutLayout>{page}</CheckoutLayout>;
};
