import CouponContextProvider from './CouponContextProvider';
import PaymentMethodContextProvider from './PaymentMethodContextProvider';
import Web3ContextProvider from './Web3ContextProvider';

interface Props {
  children: React.ReactNode;
}

export default function ContextProviders({ children }: Props) {
  return (
    <Web3ContextProvider>
      <PaymentMethodContextProvider>
        <CouponContextProvider>{children}</CouponContextProvider>
      </PaymentMethodContextProvider>
    </Web3ContextProvider>
  );
}
