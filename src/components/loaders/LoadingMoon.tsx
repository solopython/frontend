import MoonLoader from 'react-spinners/MoonLoader';

interface LoadingMoonProps {
  size: number;
  color: string;
}

export default function LoadingMoon({ size, color }: LoadingMoonProps) {
  return (
    <div className="w-full h-full flex items-center justify-center">
      <MoonLoader loading color={`${color}`} size={size} />
    </div>
  );
}
