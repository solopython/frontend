'use client';

import { Fragment, useCallback, useEffect, useState } from 'react';
import { Disclosure, Menu, Transition, Dialog } from '@headlessui/react';
import {
  ChevronDownIcon,
  FunnelIcon,
  MinusIcon,
  PlusIcon,
  Squares2X2Icon,
} from '@heroicons/react/20/solid';
import CoursesList from './CoursesList';
import { ICourseList } from '@/interfaces/courses/CoursesList';
import { MagnifyingGlassIcon, XMarkIcon } from '@heroicons/react/24/outline';
import { useDebounce } from '@/hooks/debounceHook';
import { ICategory } from '@/interfaces/category/Category';
import CourseCard from './CourseCard';
import { AnimatePresence, motion } from 'framer-motion';

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ');
}

const filters = [
  {
    id: 'color',
    name: 'Color',
    options: [
      { value: 'white', label: 'White', checked: false },
      { value: 'beige', label: 'Beige', checked: false },
      { value: 'blue', label: 'Blue', checked: true },
      { value: 'brown', label: 'Brown', checked: false },
      { value: 'green', label: 'Green', checked: false },
      { value: 'purple', label: 'Purple', checked: false },
    ],
  },
  {
    id: 'category',
    name: 'Category',
    options: [
      { value: 'new-arrivals', label: 'New Arrivals', checked: false },
      { value: 'sale', label: 'Sale', checked: false },
      { value: 'travel', label: 'Travel', checked: true },
      { value: 'organization', label: 'Organization', checked: false },
      { value: 'accessories', label: 'Accessories', checked: false },
    ],
  },
  {
    id: 'size',
    name: 'Size',
    options: [
      { value: '2l', label: '2L', checked: false },
      { value: '6l', label: '6L', checked: false },
      { value: '12l', label: '12L', checked: false },
      { value: '18l', label: '18L', checked: false },
      { value: '20l', label: '20L', checked: false },
      { value: '40l', label: '40L', checked: true },
    ],
  },
];

const sortOptions = [
  { value: 'sold', name: 'Most Popular', href: '#', current: true },
  { value: 'student_rating', name: 'Best Rating', href: '#', current: false },
  { value: 'created_at', name: 'Newest', href: '#', current: false },
];

const subCategories = [
  { name: 'Totes', href: '#' },
  { name: 'Backpacks', href: '#' },
  { name: 'Travel Bags', href: '#' },
  { name: 'Hip Bags', href: '#' },
  { name: 'Laptop Sleeves', href: '#' },
];

interface CourseSearchProps {
  categories: ICategory[];
  courses: ICourseList[];
}

export default function CourseSearch({ categories, courses }: CourseSearchProps) {
  const [mobileFiltersOpen, setMobileFiltersOpen] = useState(false);

  const [showFilter, setShowFilter] = useState(true);

  const handleFilterHide = () => {
    if (showFilter) {
      setShowFilter(false);
    } else {
      setShowFilter(true);
    }
  };
  const handleMobileFilterHide = () => {
    if (mobileFiltersOpen) {
      setMobileFiltersOpen(false);
    } else {
      setMobileFiltersOpen(true);
    }
  };

  const filterAnimation = {
    initial: { x: '-100%', opacity: 1 },
    animate: { x: 0, opacity: 1, scale: 1 },
    exit: { x: '-100%', opacity: 1 },
    transition: { duration: 0.3, ease: 'easeOut' },
  };

  const [selectedCategory, setCategory] = useState<ICategory | null>(null);
  const [searchBy, setSearchBy] = useState<string>('');
  const debouncedSearchTerm = useDebounce(searchBy, 1000);

  const [selectedSortingOption, setSelectedSortingOption] = useState(null);
  function handleSortChange(option) {
    setSelectedSortingOption(option);
  }

  const [selectedFilters, setSelectedFilters] = useState({
    price: [],
    language: [],
    level: [],
    category: [],
  });

  function handleCheckboxChange(filterType, value) {
    setSelectedFilters((prevState) => {
      // copia el estado anterior
      const newState = { ...prevState };
      // si el filtro ya está en el estado, lo remueve, de lo contrario lo agrega
      if (newState[filterType].includes(value)) {
        newState[filterType] = newState[filterType].filter((filter) => filter !== value);
      } else {
        newState[filterType].push(value);
      }
      return newState;
    });
  }

  // Esta función devuelve el total de filtros seleccionados
  function countSelectedFilters() {
    return Object.values(selectedFilters).reduce(
      (total, filterArray) => total + filterArray.length,
      0,
    );
  }

  // Esta función establece todos los filtros a un estado no seleccionado
  function clearFilters() {
    setSelectedFilters({
      price: [],
      language: [],
      level: [],
      category: [],
    });
  }

  const [newCourses, setNewCourses] = useState<ICourseList[]>([]); // New state for the search results

  const [renderedCourses, setRenderedCourses] = useState<ICourseList[]>(
    newCourses.length > 0 ? newCourses : courses,
  );

  const fetchCourses = useCallback(
    async (searchBy: string) => {
      const params = new URLSearchParams();
      Object.entries(selectedFilters).forEach(([key, values]) => {
        values.forEach((value) => {
          params.append(key, value);
        });
      });

      // Añade la opción de clasificación seleccionada a los parámetros de la solicitud
      if (selectedSortingOption) {
        params.append('sorting', selectedSortingOption);
      }

      if (searchBy) {
        params.append('search', searchBy);
      }

      if (selectedCategory) {
        params.append('category', selectedCategory.slug);
      }

      const res = await fetch(
        `${process.env.NEXT_PUBLIC_APP_API_URL}/api/courses/list/?${params.toString()}`,
      );
      const data = await res.json();
      setRenderedCourses(data.results);
    },
    [selectedFilters, selectedSortingOption, selectedCategory],
  );

  useEffect(() => {
    fetchCourses(searchBy);
  }, [fetchCourses]);

  const onSubmit = async (e: any) => {
    e.preventDefault();
    fetchCourses(searchBy);
  };

  useEffect(() => {
    if (debouncedSearchTerm) {
      fetchCourses(debouncedSearchTerm);
    }
  }, [debouncedSearchTerm]);

  const filterArea = () => (
    <form onSubmit={onSubmit} className="hidden lg:block">
      <h3 className="mb-4 text-2xl text-blue-500 font-circular-bold">Search</h3>
      <div className="flex items-center space-x-2 border-b border-gray-500 mb-8">
        <button type="submit">
          <MagnifyingGlassIcon className="h-4 w-auto mb-1" />
        </button>
        <input
          className="w-full font-circular-book py-1 placeholder-gray-400 text-gray-600 ring-none outline-none border-none focus:border-none focus:ring-none focus:outline-none"
          type="text"
          required
          name="searchBy"
          value={searchBy}
          onChange={(e) => {
            setSearchBy(e.target.value);
          }}
          placeholder="Search here..."
        />
      </div>

      <h3 className="mb-4 text-2xl text-blue-500 font-circular-bold">Categories</h3>
      <ul
        role="list"
        className="space-y-4 border-b border-gray-200 pb-6 text-sm font-medium text-gray-900"
      >
        {categories?.map((category) => (
          <li key={category.name}>
            <button
              onClick={() => {
                setCategory(category);
              }}
              type="button"
              className={`font-circular-bold text-lg ${
                selectedCategory?.slug === category.slug ? 'text-blue-400' : 'text-gray-400'
              } `}
            >
              {category.name}
            </button>
          </li>
        ))}
      </ul>
    </form>
  );

  return (
    <div className="bg-white">
      <div>
        {/* Mobile filter dialog */}
        <Transition.Root show={mobileFiltersOpen} as={Fragment}>
          <Dialog as="div" className="relative z-40 lg:hidden" onClose={setMobileFiltersOpen}>
            <Transition.Child
              as={Fragment}
              enter="transition-opacity ease-linear duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-black bg-opacity-25" />
            </Transition.Child>

            <div className="fixed inset-0 z-40 flex">
              <Transition.Child
                as={Fragment}
                enter="transition ease-in-out duration-300 transform"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transition ease-in-out duration-300 transform"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <Dialog.Panel className="relative ml-auto flex h-full w-full max-w-xs flex-col overflow-y-auto bg-white py-4 pb-12 shadow-xl">
                  <div className="flex items-center justify-between px-4">
                    <h2 className="text-lg font-medium text-gray-900">Filters</h2>
                    <button
                      type="button"
                      className="-mr-2 flex h-10 w-10 items-center justify-center rounded-md bg-white p-2 text-gray-400"
                      onClick={() => setMobileFiltersOpen(false)}
                    >
                      <span className="sr-only">Close menu</span>
                      <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                    </button>
                  </div>

                  {/* Filters */}
                  <div className="p-4">{filterArea()}</div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition.Root>

        <main className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
          <div className="flex items-baseline justify-between border-b border-gray-200 pb-6 pt-24">
            <h1 className="text-4xl font-circular-bold tracking-tight text-gray-900">
              Search our courses
            </h1>

            <div className="flex items-center">
              <Menu as="div" className="relative inline-block text-left">
                <div>
                  <Menu.Button className="group inline-flex justify-center text-sm font-medium text-gray-700 hover:text-gray-900">
                    Sort
                    <ChevronDownIcon
                      className="-mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                      aria-hidden="true"
                    />
                  </Menu.Button>
                </div>

                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="absolute right-0 z-10 mt-2 w-40 origin-top-right rounded-md bg-white shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <div className="py-1">
                      {sortOptions.map((option) => (
                        <Menu.Item key={option.value}>
                          {({ active }) => (
                            <button
                              onClick={() => handleSortChange(option.value)}
                              className={classNames(
                                selectedSortingOption === option.value
                                  ? 'font-medium text-gray-900'
                                  : 'text-gray-500',
                                active ? 'bg-gray-100' : '',
                                'block px-4 py-2 text-sm w-full text-left',
                              )}
                            >
                              {option.name}
                            </button>
                          )}
                        </Menu.Item>
                      ))}
                    </div>
                  </Menu.Items>
                </Transition>
              </Menu>

              <button
                type="button"
                onClick={() => {
                  handleFilterHide();
                  handleMobileFilterHide();
                }}
                className="-m-2 ml-5 p-2 text-gray-400 hover:text-gray-500 sm:ml-7"
              >
                <span className="sr-only">View grid</span>
                <Squares2X2Icon className="h-5 w-5" aria-hidden="true" />
              </button>
              <button
                type="button"
                className="-m-2 ml-4 p-2 text-gray-400 hover:text-gray-500 sm:ml-6 lg:hidden"
                onClick={() => setMobileFiltersOpen(true)}
              >
                <span className="sr-only">Filters</span>
                <FunnelIcon className="h-5 w-5" aria-hidden="true" />
              </button>
            </div>
          </div>

          <section aria-labelledby="products-heading" className="pb-24 pt-6">
            <h2 id="products-heading" className="sr-only">
              Products
            </h2>

            <div className="grid grid-cols-1 gap-x-8 gap-y-10 lg:grid-cols-4">
              {/* Filters */}
              <AnimatePresence>
                {showFilter ? (
                  <motion.div key="filters" {...filterAnimation} className="hidden lg:block">
                    {filterArea()}
                  </motion.div>
                ) : (
                  <div />
                )}
              </AnimatePresence>

              {/* Product grid */}
              <motion.div
                id="product-grid"
                className={`${showFilter ? 'lg:col-span-3' : 'lg:col-span-4'}`}
                transition={{ duration: 0.3 }}
              >
                {/* Replace with your content */}
                <div className="w-full flex ">
                  {/* Product grid */}
                  <ul>
                    {renderedCourses.map((course) => (
                      <CourseCard key={course.slug} course={course} />
                    ))}
                  </ul>
                </div>
              </motion.div>
              {/* /End replace */}
            </div>
          </section>
        </main>
      </div>
    </div>
  );
}
