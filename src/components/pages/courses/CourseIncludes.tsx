import { ICourseDetail } from '@/interfaces/courses/CourseDetail';

export default function CourseIncludes({ course }: { course: ICourseDetail }) {
  return (
    <div>
      <div className="items-left text-sm mt-4 my-3 text-left font-bold dark:text-dark-txt">
        This course includes:
      </div>
      <div className="space-y-1">
        <li className="flex items-center">
          <i className="bx bx-movie-play rounded-full text-xl" />
          <div className="ml-3">
            <p className="text-xs font-medium dark:text-dark-txt-secondary text-gray-900">
              {course?.total_duration} Videos
            </p>
          </div>
        </li>
        <li className="flex items-center">
          <i className="bx bx-file-blank rounded-full text-xl" />
          <div className="ml-3">
            <p className="text-xs font-medium dark:text-dark-txt-secondary text-gray-900">
              {course?.total_lectures} Articles
            </p>
          </div>
        </li>
        <li className="flex items-center">
          <i className="bx bx-infinite rounded-full text-xl" />
          <div className="ml-3">
            <p className="text-xs font-medium dark:text-dark-txt-secondary text-gray-900">
              Unlimited access
            </p>
          </div>
        </li>
        <li className="flex items-center">
          <i className="bx bx-notepad rounded-full text-xl" />
          <div className="ml-3">
            <p className="text-xs font-medium dark:text-dark-txt-secondary text-gray-900">
              Assignments
            </p>
          </div>
        </li>
        <li className="flex items-center">
          <i className="bx bx-trophy rounded-full text-xl" />
          <div className="ml-3">
            <p className="text-xs font-medium dark:text-dark-txt-secondary text-gray-900">
              Certificate of completion
            </p>
          </div>
        </li>
      </div>
    </div>
  );
}
