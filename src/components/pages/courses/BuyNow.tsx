import { useContext } from 'react';
import CartContext from '@/context/cartContext';
import { RootState } from '@/redux/reducers';
import { useSelector } from 'react-redux';
import { AddToCartProps, addToCart } from '@/utils/api/addToCart';
import CouponContext from '@/context/couponContext';

export default function BuyNow({ course }) {
  const user = useSelector((state: RootState) => state.auth.user);

  const { setCartItems, setTotalItems } = useContext(CartContext);
  const { id, name, fixedPriceCoupon, percentageCoupon, contentType, objectId, uses } =
    useContext(CouponContext);
  const coupon = {
    id,
    name,
    fixedPriceCoupon,
    percentageCoupon,
    uses,
    objectId,
    contentType,
  };

  // const courseExistsInCart = items && items.some((u) => u.course && u.course.includes(course?.id));

  async function handleAddToCart(e: any) {
    e.preventDefault();

    if (user) {
      try {
        const addToCartData: AddToCartProps = {
          itemID: course?.id,
          type: 'Course',
          coupon: coupon || null,
          shipping: null,
          quantity: null,
          size: null,
          color: null,
          weight: null,
          material: null,
          referrer: null,
        };

        const res = await addToCart(addToCartData);
        setCartItems(res.cart);
        setTotalItems(res.total_items);
        // Update the context with the new cart data here
      } catch (error) {
        console.error('Error adding to cart:', error);
      }
    } else {
      let cart = [];

      // Retrieve cart from localStorage
      if (localStorage.getItem('cart')) {
        cart = JSON.parse(localStorage.getItem('cart'));
      }

      // Add course to cart if it's not already there
      const exists = cart.find((item) => item.course.id === course?.id);
      if (!exists) {
        const newItem = {
          course: {
            id: course?.id,
            title: course?.title,
            slug: course?.slug,
            short_description: course?.short_description,
            price: course?.price,
            thumbnail: course?.images[0].file,
          },
          type: 'Course',
          coupon,
          referrer: '',
        };
        cart.push(newItem);
      }

      // Store updated cart back in localStorage
      localStorage.setItem('cart', JSON.stringify(cart));
      setCartItems(cart);
      setTotalItems(cart.length);
    }
  }

  return (
    <button
      //   onClick={(e) => {
      //     handleAddToCart(e);
      //   }}
      className="bg-white w-full hover:bg-gray-100 text-black border border-gray-900 font-bold py-3 px-4 rounded"
    >
      Buy Now
    </button>
  );
}
