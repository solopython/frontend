import React from 'react';
import LoadingMoon from '@/components/loaders/LoadingMoon';
import RelatedCoursesList from './RelatedCoursesList';

function RelatedCourses({ courses }) {
  return (
    <div className="max-w-full">
      <div className="flex items-center justify-between space-x-4">
        <h2 className="mb-4 text-2xl font-black text-gray-900 dark:text-dark-txt">
          Students also bought
        </h2>
        {/* Button here */}
      </div>
      <RelatedCoursesList courses={courses} />
    </div>
  );
}

export default RelatedCourses;
