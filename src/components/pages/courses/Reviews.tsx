import React, { Fragment } from 'react';
import { Menu, Transition } from '@headlessui/react';
import {} from '@heroicons/react/24/outline';
import { ChevronDownIcon, StarIcon } from '@heroicons/react/20/solid';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Reviews({ course }) {
  const filterReviews = (numStars) => {
    // filter_reviews(courseUUID, numStars)
  };

  const getReviews = () => {
    // get_reviews(courseUUID)
  };

  return (
    <div className="">
      <div className="mx-auto max-w-5xl lg:max-w-7xl">
        <div className=" py-2 sm:flex sm:items-center sm:justify-between">
          {/* <StarIcon
                  className="inline-flex h-7 w-7 flex-shrink-0 text-yellow-500"
                /> */}
          <h1 className="font-gilroy-black text-2xl tracking-tight text-gray-900 dark:text-dark-txt">
            Reviews{' '}
          </h1>
          <div className="  sm:ml-4">
            {course?.student_rating_no} Ratings
            <Menu as="div" className="relative z-10 inline-block text-left">
              <div>
                <Menu.Button className="font-gilroy-semibold group inline-flex justify-center  rounded-xl px-2 py-1.5 text-sm text-gray-700  hover:text-gray-900 dark:border-dark-second dark:text-dark-txt">
                  Filter
                  <ChevronDownIcon
                    className="-mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500 dark:text-dark-txt"
                    aria-hidden="true"
                  />
                </Menu.Button>
              </div>

              <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <Menu.Items className="absolute right-0 z-10 mt-2 w-40 origin-top-left rounded-md bg-white shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none dark:bg-dark-main">
                  <div className="py-1">
                    <Menu.Item>
                      <button
                        type="button"
                        className="btn btn-primary btn-sm font-gilroy-light mb-3 ml-6 mt-2 dark:text-dark-txt"
                        onClick={getReviews}
                      >
                        Show all
                      </button>
                    </Menu.Item>
                    <Menu.Item>
                      <button
                        type="button"
                        className="mb-1 text-center"
                        style={{ cursor: 'pointer' }}
                        onClick={() => filterReviews(5)}
                      >
                        <StarIcon className="text-almond-600" rating={5.0} />
                      </button>
                    </Menu.Item>
                    <Menu.Item>
                      <button
                        type="button"
                        className="mb-1 text-center"
                        style={{ cursor: 'pointer' }}
                        onClick={() => filterReviews(4.0)}
                      >
                        <StarIcon className="text-almond-600" rating={4.0} />
                      </button>
                    </Menu.Item>
                    <Menu.Item>
                      <button
                        type="button"
                        className="mb-1 text-center"
                        style={{ cursor: 'pointer' }}
                        onClick={() => filterReviews(3.0)}
                      >
                        <StarIcon className="text-almond-600" rating={3.0} />
                      </button>
                    </Menu.Item>
                    <Menu.Item>
                      <button
                        type="button"
                        className="mb-1 text-center"
                        style={{ cursor: 'pointer' }}
                        onClick={() => filterReviews(2.0)}
                      >
                        <StarIcon className="text-almond-600" rating={2.0} />
                      </button>
                    </Menu.Item>
                    <Menu.Item>
                      <button
                        type="button"
                        className="mb-1 text-center"
                        style={{ cursor: 'pointer' }}
                        onClick={() => filterReviews(1.0)}
                      >
                        <StarIcon className="text-almond-600" rating={1.0} />
                      </button>
                    </Menu.Item>
                  </div>
                </Menu.Items>
              </Transition>
            </Menu>
          </div>
        </div>
        REVIEWS SEC
      </div>
    </div>
  );
}
