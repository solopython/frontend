import { ICourseList } from '@/interfaces/courses/CoursesList';
import Link from 'next/link';
import slugify from 'react-slugify';
import { StarIcon } from '@heroicons/react/20/solid';
import { useState } from 'react';

interface CourseCardProps {
  course: ICourseList;
}

export default function CourseCard({ course }: CourseCardProps) {
  const [hover, setHover] = useState(false);

  const handleMouseEnter = () => {
    setHover(true);
  };
  const handleMouseLeave = () => {
    setHover(false);
  };

  return (
    <div>
      <div
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        className="block lg:flex lg:flex-row justify-between items-start shadow-neubrutalism-xl ring-2 w-full ring-gray-900 shadow-blue-100 hover:-translate-x-0.5  hover:-translate-y-0.5 transition duration-300 ease-in-out rounded-2xl mt-4 "
      >
        <Link href={`/courses/view/${course.slug}`}>
          <img
            className="lg:hidden flex rounded-lg object-cover w-full h-40"
            src={course?.thumbnail}
            alt=""
          />
        </Link>
        <Link href={`/courses/view/${course.slug}`}>
          <img
            className="hidden lg:flex rounded-lg m-1 object-cover w-auto h-20 lg:h-40"
            src={course?.thumbnail}
            alt=""
          />
        </Link>

        <div className="flex lg:block w-full items-center space-x-4">
          <div className="block ">
            <div className="mt-4 mx-4 flex flex-wrap items-center justify-between sm:flex-nowrap">
              <div className=" flex items-center space-x-2">
                <Link href={`/courses/view/${course.slug}`}>
                  <p
                    className={`text-md font-circular-book font-bold ${
                      hover
                        ? 'text-blue-500 dark:text-dark-primary'
                        : 'dark:text-dark-txt text-gray-800 '
                    }`}
                  >
                    {course.title.slice(0, 79)}
                  </p>
                </Link>
              </div>
              <Link
                href={`/categories/view/${slugify(course.category)}`}
                className="text-sm text-gray-800 px-4 py-2 font-circular-light bg-gray-50 rounded-full hover:bg-gray-100"
              >
                {course.category}
              </Link>{' '}
              <div className=" flex-shrink-0 ">
                <div className="flex flex-row items-center">
                  <StarIcon className="h-5 w-auto text-yellow-500" />
                  <p className="text-sm">{course?.student_rating}</p>
                </div>
              </div>
            </div>
            <div className="flex mx-4 flex-row justify-between items-start mt-4">
              <div>
                <p className="text-sm  text-gray-800 font-circular-light mt-2">
                  {course?.short_description?.slice(0, 149)}
                </p>
                <p className="text-sm text-gray-800 mt-2 flex gap-x-1 font-circular-bold">
                  {' '}
                  S/ <strong>{course?.price}</strong>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
