import { useContext } from 'react';
import CartContext from '@/context/cartContext';
import { RootState } from '@/redux/reducers';
import { useDispatch, useSelector } from 'react-redux';
import { AddToCartProps, addToCart } from '@/utils/api/addToCart';
import CouponContext from '@/context/couponContext';
import { addCartItem, addCartItemAnonymous } from '@/redux/actions/cart/actions';
import { AddToCartAnonymousProps } from '@/redux/actions/cart/interfaces';
import { ICourseDetail } from '@/interfaces/courses/CourseDetail';

interface PageProps {
  course: ICourseDetail;
  referrer: string;
}

export default function AddToCart({ course, referrer }: PageProps) {
  const user = useSelector((state: RootState) => state.auth.user);

  const { setCartItems, setTotalItems } = useContext(CartContext);
  const { id, name, fixedPriceCoupon, percentageCoupon, contentType, objectId, uses } =
    useContext(CouponContext);
  const coupon = {
    id,
    name,
    fixedPriceCoupon,
    percentageCoupon,
    uses,
    objectId,
    contentType,
  };

  const dispatch = useDispatch();

  // const courseExistsInCart = items && items.some((u) => u.course && u.course.includes(course?.id));

  async function handleAddToCart(e: any) {
    e.preventDefault();

    if (user) {
      try {
        const addToCartData: AddToCartProps = {
          itemID: course?.id,
          type: 'Course',
          coupon: coupon || null,
          shipping: null,
          quantity: null,
          size: null,
          color: null,
          weight: null,
          material: null,
          referrer: null,
        };

        dispatch(addCartItem(addToCartData));
      } catch (error) {
        console.error('Error adding to cart:', error);
      }
    } else {
      const addToCartData: AddToCartAnonymousProps = {
        item: course,
        type: 'Course',
        coupon: coupon || null,
        shipping: null,
        quantity: null,
        size: null,
        color: null,
        weight: null,
        material: null,
        referrer: referrer || null,
      };

      dispatch(addCartItemAnonymous(addToCartData));
    }
  }

  return (
    <button
      onClick={(e) => {
        handleAddToCart(e);
      }}
      className="bg-purple-600 w-full hover:bg-purple-700 text-white font-bold py-3 px-4 rounded"
    >
      Add to Cart
    </button>
  );
}
