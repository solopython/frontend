import { initMercadoPago, CardPayment } from '@mercadopago/sdk-react';
import React from 'react';

import { ToastSuccess } from '@/components/toast/ToastSuccess';
import PaymentMethodContext from '@/context/paymentMethodContext';
import { RootState } from '@/redux/reducers';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/navigation';
import { useContext, useEffect, useState } from 'react';
import StatusBrick from './StatusBrick';
import { useDispatch } from 'react-redux';
import { emptyCartAuthenticated } from '@/redux/actions/cart/actions';

initMercadoPago(`${process.env.NEXT_PUBLIC_APP_MERCADOPAGO_PUBLIC_KEY}`, {
  locale: 'es-PE',
});

export default function CreditCardPayment() {
  const router = useRouter();

  const totalPrice = useSelector((state: RootState) => state.cart.finalPrice);
  const items = useSelector((state: RootState) => state.cart.items);

  const { value, status, setStatus } = useContext(PaymentMethodContext);

  const initialization = {
    amount: totalPrice,
  };

  const dispatch = useDispatch();
  const onSubmit = async (formData) => {
    // Append cartData to formData
    formData.cart = items;
    const body = JSON.stringify(formData);

    const res = await fetch(`/api/checkout/pay`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body,
    });

    if (res.status === 200) {
      setStatus(true);
      ToastSuccess('Payment Successful!');
      dispatch(emptyCartAuthenticated());
      // window.scrollTo(0, 0);
      setTimeout(() => {
        router.push('/library');
      }, 6000);
    }
  };

  const onError = async (error) => {
    // callback llamado para todos los casos de error de Brick
    console.log(error);
  };

  const onReady = async () => {
    /*
      Callback llamado cuando Brick está listo.
      Aquí puedes ocultar cargamentos de su sitio, por ejemplo.
    */
  };

  useEffect(() => {
    return () => {
      setStatus(false);
    };
  }, [setStatus]);

  return (
    <div>
      {value?.value === 'credit' && status === false ? (
        <CardPayment
          initialization={initialization}
          onSubmit={onSubmit}
          onReady={onReady}
          onError={onError}
        />
      ) : (
        <StatusBrick />
      )}
    </div>
  );
}
