import Logo from '@/features/navbar/Logo';
import { useRouter } from 'next/navigation';

export default function Navbar() {
  const router = useRouter();
  return (
    <>
      <h1 className="bg-blue-500 w-full  pt-1 pb-0.5"></h1>

      <header
        className="border-b border-gray-200 bg-white py-4 w-full z-40 mx-auto flex max-w-full items-center justify-between gap-x-2 lg:px-20
"
      >
        <div className="relative w-full flex justify-between md:gap-8">
          <div className="flex sm:absolute md:inset-y-0 md:left-0 md:static">
            <div className="flex items-center">
              <Logo />
            </div>
          </div>
          <div className="min-w-0 flex-1 sm:px-8 md:px-0 flex items-center w-full py-3 md:mx-auto md:max-w-full lg:mx-0 lg:max-w-none">
            <div />
          </div>
          <div className="flex items-center justify-end space-x-4">
            <div className="hidden lg:inline-flex items-center text-sm dark:text-dark-txt text-gray-900 hover:text-iris-600">
              <button
                type="button"
                onClick={() => {
                  router.back();
                }}
                className="font-bold dark:text-dark-accent dark:hover:text-dark-primary text-iris-500"
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </header>
    </>
  );
}
