import CartItem from '@/components/cart/CartItem';
import TierCartItem from '@/components/cart/TierCartItem';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux/reducers';
import {
  emptyCartAnonymous,
  emptyCartAuthenticated,
  getCartTotal,
  getItems,
} from '@/redux/actions/cart/actions';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

export default function OrderItems() {
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
  const items = useSelector((state: RootState) => state.cart.items);

  const dispatch = useDispatch();

  const handleClearCart = async () => {
    if (isAuthenticated) {
      dispatch(emptyCartAuthenticated());
    } else {
      dispatch(emptyCartAnonymous());
    }
  };

  useEffect(() => {
    if (isAuthenticated) {
      dispatch(getItems());
    }
  }, []);

  useEffect(() => {
    dispatch(getCartTotal(items));
  }, [items]);

  return (
    <>
      {items?.length > 0 && (
        <div className="flow-root">
          <ul role="list" className="-my-6 divide-y divide-gray-200">
            {items.map((item, idx) => {
              if (item.tier) {
                return <TierCartItem item={item} key={idx} />;
              } else if (item.course) {
                return <CartItem item={item} key={idx} />;
              }
              // Add more conditions for other item types if needed
              return null;
            })}
          </ul>
        </div>
      )}{' '}
      {items?.length === 0 && (
        <div className="my-4 text-center">
          <svg
            className="mx-auto h-12 w-12 text-gray-400 dark:text-dark-txt"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            aria-hidden="true"
          >
            <path
              vectorEffect="non-scaling-stroke"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
            />
          </svg>
          <p className="mt-2 text-sm font-semibold text-gray-900 dark:text-white">
            No hay productos
          </p>
        </div>
      )}
    </>
  );
}
