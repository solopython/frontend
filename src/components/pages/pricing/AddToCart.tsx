import CouponContext from '@/context/couponContext';
import { ITier } from '@/interfaces/tiers/Tier';
import { RootState } from '@/redux/reducers';
import { useDispatch, useSelector } from 'react-redux';
import { useContext } from 'react';
import { AddToCartProps } from '@/utils/api/addToCart';
import { addCartItem, addCartItemAnonymous } from '@/redux/actions/cart/actions';
import { AddToCartAnonymousProps } from '@/redux/actions/cart/interfaces';

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ');
}

interface ComponentProps {
  tier: ITier;
}

export default function AddToCart({ tier }: ComponentProps) {
  const user = useSelector((state: RootState) => state.auth.user);
  const items = useSelector((state: RootState) => state.cart.items);

  const { id, name, fixedPriceCoupon, percentageCoupon, contentType, objectId, uses } =
    useContext(CouponContext);

  const coupon = {
    id,
    name,
    fixedPriceCoupon,
    percentageCoupon,
    uses,
    objectId,
    contentType,
  };

  const dispatch = useDispatch();

  async function handleAddToCart(e: any) {
    e.preventDefault();

    if (user) {
      try {
        const addToCartData: AddToCartProps = {
          itemID: tier.id,
          type: 'Tier',
          coupon: coupon || null,
          shipping: null,
          quantity: null,
          size: null,
          color: null,
          weight: null,
          material: null,
          referrer: null,
        };

        dispatch(addCartItem(addToCartData));
        // Update the context with the new cart data here
      } catch (error) {
        console.error('Error adding to cart:', error);
      }
    } else {
      const addToCartData: AddToCartAnonymousProps = {
        item: tier,
        type: 'Tier',
        coupon: coupon || null,
        shipping: null,
        quantity: null,
        size: null,
        color: null,
        weight: null,
        material: null,
        referrer: null,
      };

      dispatch(addCartItemAnonymous(addToCartData));
    }
  }

  const itemExistsInCart = items?.some((u: any) => u?.tier?.id === tier?.id);

  return (
    <>
      {itemExistsInCart ? (
        <button
          className={classNames(
            tier.mostPopular
              ? 'bg-blue-500 hover:bg-blue-600 text-white border-2 border-blue-500 hover:border-blue-500'
              : 'border-2 border-gray-900',
            'transition duration-300 ease-in-out scale-100 hover:scale-105 w-full mt-6 block rounded-lg py-3 px-3 text-center text-lg font-circular-bold leading-6 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600',
          )}
        >
          Go to checkout
        </button>
      ) : (
        <button
          onClick={(e) => {
            handleAddToCart(e);
          }}
          className={classNames(
            tier.mostPopular
              ? 'bg-blue-500 hover:bg-blue-600 text-white border-2 border-blue-500 hover:border-blue-500'
              : 'border-2 border-gray-900',
            'transition duration-300 ease-in-out scale-100 hover:scale-105 w-full mt-6 block rounded-lg py-3 px-3 text-center text-lg font-circular-bold leading-6 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600',
          )}
        >
          Start for Free
        </button>
      )}
    </>
  );
}
