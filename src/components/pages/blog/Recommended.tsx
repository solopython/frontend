import Button from '@/components/Button';
import { IPost } from '@/interfaces/blog/Post';
import moment from 'moment';
import Link from 'next/link';

interface ComponentProps {
  posts: IPost[];
}

export default function Recommended({ posts }: ComponentProps) {
  return (
    <div className="bg-blue-100 bg-opacity-10 py-8">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className=" py-5 ">
          <div className="-ml-4 -mt-2 flex flex-wrap items-center justify-between sm:flex-nowrap">
            <div className="ml-4 mt-2">
              <h3 className="text-2xl lg:text-3xl font-circular-medium leading-6 text-blue-500">
                Grow Your Business
              </h3>
            </div>
            <div className="ml-4 mt-2 flex-shrink-0">
              <Button>See all articles</Button>
            </div>
          </div>
        </div>
        <div className="mx-auto grid max-w-2xl grid-cols-1 gap-x-8 gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-3">
          {posts?.map((post) => (
            <article key={post.slug} className="flex flex-col items-start justify-between">
              <div className="relative w-full">
                <img
                  src={post?.thumbnail}
                  alt=""
                  className="aspect-[16/9] h-56 w-full rounded bg-gray-100 object-cover sm:aspect-[2/1] lg:aspect-[3/2]"
                />
              </div>
              <div className="max-w-xl">
                <div className="mt-4 flex items-center gap-x-4 text-sm">
                  <Link
                    href={`/categories/view/${post?.category?.slug}`}
                    className="relative z-10 rounded-full  font-circular-book text-blue-600"
                  >
                    {post?.category?.name}
                  </Link>
                  <time dateTime={post?.published} className="text-gray-500">
                    {moment(post?.published).calendar()}
                  </time>
                </div>
                <div className="group relative">
                  <h3 className="mt-3 text-lg  leading-6 text-gray-900 group-hover:text-gray-600">
                    <Link href={`/blog/post/${post?.slug}`} className="font-circular-medium">
                      <span className="absolute inset-0" />
                      {post?.title}
                    </Link>
                  </h3>
                  <Link href={`/blog/post/${post?.slug}`} className="">
                    <Button className="mt-8 bg-white border-2 hover:bg-gray-50 border-blue-500 ">
                      <span className="text-blue-500 font-circular-medium">Read More</span>
                    </Button>
                  </Link>
                </div>
              </div>
            </article>
          ))}
        </div>
      </div>
    </div>
  );
}
