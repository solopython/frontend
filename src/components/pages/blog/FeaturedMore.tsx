import Button from '@/components/Button';
import { IPost } from '@/interfaces/blog/Post';
import moment from 'moment';
import Link from 'next/link';

interface ComponentProps {
  posts: IPost[];
}

export default function FeaturedMore({ posts }: ComponentProps) {
  return (
    <div>
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className=" pt-5 ">
          <div className="-ml-4 -mt-2 flex flex-wrap items-center justify-between sm:flex-nowrap">
            <div className="ml-4 mt-2">
              <h3 className="text-2xl lg:text-3xl font-circular-medium leading-6 text-blue-500">
                Featured Articles
              </h3>
            </div>
            <div className="ml-4 mt-2 flex-shrink-0">
              <Button>See all articles</Button>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-white pb-16">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto max-w-4xl lg:max-w-7xl">
            <div className="mx-auto mt-16 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-2">
              {posts?.map((post) => (
                <article
                  key={post.slug}
                  className="relative isolate flex flex-col gap-8 lg:flex-row"
                >
                  <div className="relative aspect-[16/9] sm:aspect-[2/1] lg:aspect-square lg:w-64 lg:shrink-0">
                    <img
                      src={post?.thumbnail}
                      alt=""
                      className="absolute inset-0 h-full w-full rounded-2xl bg-gray-50 object-cover"
                    />
                    <div className="absolute inset-0 rounded-2xl ring-1 ring-inset ring-gray-900/10" />
                  </div>
                  <div>
                    <div className="flex items-center gap-x-4 text-xs">
                      <Link
                        href={`/categories/view/${post?.category?.slug}`}
                        className="relative z-10 rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100"
                      >
                        {post?.category?.name}
                      </Link>
                      <time dateTime={post?.published} className="text-gray-500">
                        {moment(post?.published).calendar()}
                      </time>
                    </div>
                    <div className="group relative max-w-xl">
                      <h3 className="mt-3 text-lg font-semibold leading-6 text-gray-900 group-hover:text-gray-600">
                        <Link href={`/blog/post/${post?.slug}`}>
                          <span className="absolute inset-0" />
                          {post?.title}
                        </Link>
                      </h3>
                      <Link href={`/blog/post/${post?.slug}`}>
                        <Button className="mt-2 bg-white border-2 hover:bg-gray-50 border-blue-500 ">
                          <span className="text-blue-500 font-circular-medium">Read More</span>
                        </Button>
                      </Link>
                    </div>
                  </div>
                </article>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
