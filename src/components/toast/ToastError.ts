import { toast, ToastPosition } from 'react-toastify';

const config = {
  position: 'bottom-right' as ToastPosition,
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: false,
  progress: undefined,
  className: 'toast-error-message',
};

export const ToastError = (msg: string): void => {
  toast.error(msg, config);
};
