import Link from 'next/link';
import { Fragment, useEffect, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { ShoppingCartIcon, XMarkIcon } from '@heroicons/react/24/outline';
import { RootState } from '@/redux/reducers';
import { useDispatch, useSelector } from 'react-redux';
import CartItem from './CartItem';
import TierCartItem from './TierCartItem';
import {
  emptyCartAnonymous,
  emptyCartAuthenticated,
  getCartTotal,
  getItems,
  synchCartAuthenticated,
} from '@/redux/actions/cart/actions';
import { loginCompleted, openLoginModal } from '@/redux/actions/auth/actions';
import { SynchCartProps } from '@/redux/actions/cart/interfaces';
import { usePathname, useRouter } from 'next/navigation';

export default function ShoppingCart() {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);

  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
  const isLoginCompleted = useSelector((state: RootState) => state.auth.isLoginCompleted);
  const items = useSelector((state: RootState) => state.cart.items);
  const totalItems = useSelector((state: RootState) => state.cart.total_items);
  const totalPrice = useSelector((state: RootState) => state.cart.amount);

  const router = useRouter();
  const pathname = usePathname();

  const handleClick = () => {
    router.push({
      pathname: pathname,
      query: { ...router.query, login: 'True' },
    });
    setOpen(false);
  };

  const handleClearCart = async () => {
    if (isAuthenticated) {
      dispatch(emptyCartAuthenticated());
    } else {
      dispatch(emptyCartAnonymous());
    }
  };

  // useEffect(() => {
  //   if (isAuthenticated) {
  //     dispatch(getItems());
  //   }
  // }, []);

  // useEffect(() => {
  //   dispatch(getCartTotal(items));
  // }, [items]);

  useEffect(() => {
    if (isAuthenticated && isLoginCompleted) {
      const data: SynchCartProps = {
        items,
      };
      dispatch(synchCartAuthenticated(data));
      dispatch(loginCompleted());
    }
  }, [isAuthenticated, isLoginCompleted]);

  return (
    <>
      <button
        className="relative group items-center justify-center -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-800 hover:text-gray-500 dark:hover:text-white"
        onClick={() => {
          setOpen(true);
        }}
      >
        <ShoppingCartIcon className="h-5 w-5 text-gray-500" aria-hidden="true" />
        {totalItems > 0 && (
          <div
            className="
              absolute
              top-0
              right-0
              h-5
              w-5
              bg-red-600
              rounded-full
              flex
              items-center
              justify-center
              text-white
              text-xs
            "
          >
            {totalItems}
          </div>
        )}
      </button>

      <Transition.Root show={open} as={Fragment}>
        <Dialog as="div" className="relative z-50" onClose={setOpen}>
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-hidden">
            <div className="absolute inset-0 overflow-hidden">
              <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10">
                <Transition.Child
                  as={Fragment}
                  enter="transform transition ease-in-out duration-500 sm:duration-700"
                  enterFrom="translate-x-full"
                  enterTo="translate-x-0"
                  leave="transform transition ease-in-out duration-500 sm:duration-700"
                  leaveFrom="translate-x-0"
                  leaveTo="translate-x-full"
                >
                  <Dialog.Panel className="pointer-events-auto w-screen max-w-md">
                    <div className="flex h-full flex-col overflow-y-scroll bg-white shadow-xl">
                      <div className="flex-1 overflow-y-auto px-4 py-6 sm:px-6">
                        <div className="flex items-start justify-between">
                          <Dialog.Title className="text-lg font-medium text-gray-900">
                            Shopping cart
                          </Dialog.Title>

                          <div className="ml-3 flex h-7 items-center space-x-2">
                            {items?.length > 0 && (
                              <button
                                type="button"
                                onClick={() => {
                                  if (window.confirm('Are you sure you want to clear your cart?')) {
                                    handleClearCart();
                                  }
                                }}
                                className="cursor-pointer text-sm font-medium dark:text-dark-accent text-iris-500 hover:text-iris-600"
                              >
                                Clear cart
                              </button>
                            )}
                            <button
                              type="button"
                              className="-m-2 p-2 text-gray-400 hover:text-gray-500"
                              onClick={() => setOpen(false)}
                            >
                              <span className="sr-only">Close panel</span>
                              <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                          </div>
                        </div>

                        <div className="mt-8">
                          <div className="flow-root">
                            <ul role="list" className="-my-6 divide-y divide-gray-200">
                              {items?.map((item, idx) => {
                                if (item.tier) {
                                  return <TierCartItem item={item} key={idx} />;
                                } else if (item.course) {
                                  return <CartItem item={item} key={idx} />;
                                }
                                // Add more conditions for other item types if needed
                                return null;
                              })}
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="border-t border-gray-200 px-4 py-6 sm:px-6">
                        <div className="flex justify-between text-base font-medium text-gray-900">
                          <p>Subtotal</p>
                          <p>S/ {totalPrice}</p>
                        </div>
                        <p className="mt-0.5 text-sm text-gray-500">
                          Shipping and taxes calculated at checkout.
                        </p>
                        <div className="mt-6">
                          {isAuthenticated ? (
                            <Link
                              href="/checkout"
                              onClick={() => setOpen(false)}
                              className="flex items-center justify-center rounded-md border border-transparent bg-blue-600 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-blue-700"
                            >
                              Checkout
                            </Link>
                          ) : (
                            <Link
                              href={{
                                pathname: pathname,
                                query: { ...router.query, login: 'True' },
                              }}
                              onClick={handleClick}
                              className="flex w-full items-center justify-center rounded-md border border-transparent bg-blue-600 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-blue-700"
                            >
                              Checkout
                            </Link>
                          )}
                        </div>
                        <div className="mt-6 flex justify-center text-center text-sm text-gray-500">
                          <p>
                            or
                            <button
                              type="button"
                              className=" ml-1 font-medium text-blue-600 hover:text-blue-500"
                              onClick={() => setOpen(false)}
                            >
                              Continue Shopping
                              <span aria-hidden="true"> &rarr;</span>
                            </button>
                          </p>
                        </div>
                      </div>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
}
