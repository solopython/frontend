import { ICategory } from '@/interfaces/category/Category';
import CategoryCard from './CategoryCard';
import slugify from 'react-slugify';
import Link from 'next/link';

interface CaategoriesListProps {
  categories: ICategory[];
}

export default function CategoriesList({ categories }: CaategoriesListProps) {
  return (
    <div className="bg-gray-50">
      <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        {/* We've used 3xl here, but feel free to try other max-widths based on your needs */}
        <div className="mx-auto max-w-7xl">
          {/* Content goes here */}
          <div className="relative mt-8 pt-28 pb-16">
            <div className="relative -mb-6 w-full overflow-x-auto pb-6">
              <ul
                role="list"
                className="mx-4 inline-flex space-x-8 sm:mx-6 lg:mx-0 lg:grid lg:grid-cols-4 lg:gap-x-8 lg:space-x-0 p-2"
              >
                {categories.map((category) => (
                  <li
                    key={category.id}
                    className="inline-flex w-64 bg-blue-500 scale-100 hover:scale-105 hover:shadow-neubrutalism-md hover:shadow-blue-100 transition duration-300 ease-in-out rounded-2xl flex-col text-center lg:w-auto"
                  >
                    <Link href={`/categories/view/${slugify(category.slug)}`}>
                      <div className="group relative ">
                        <img
                          src="https://a0.muscache.com/im/pictures/a433b4d0-8183-4523-b4c5-99b81c5729c1.jpg?im_w=320"
                          className="rounded-t-xl max-h-44 w-full"
                        />
                        <p className="text-xl md:text-3xl font-circular-book text-gray-50 pt-5 pl-3">
                          {' '}
                          {category.name}{' '}
                        </p>
                        <p className="text-xs md:text-lg font-circular-light text-gray-50 pt-3 pl-3 pb-10">
                          {category.views} Views
                        </p>
                      </div>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
