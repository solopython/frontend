import { ICategory } from '@/interfaces/category/Category';
import Image from 'next/image';
import Link from 'next/link';

interface CategoryCardProps {
  category: ICategory;
}

export default function CategoryCard({ category }: CategoryCardProps) {
  return (
    <Link
      className={`col-span-3 bg-blue-500 rounded-xl h-52 md:h-80 scale-100 hover:scale-105 hover:shadow-neubrutalism-md hover:shadow-blue-100 transition duration-300 ease-in-out`}
      href={`/categories/view/${category.slug}`}
    >
      <img
        src="https://a0.muscache.com/im/pictures/a433b4d0-8183-4523-b4c5-99b81c5729c1.jpg?im_w=320"
        className="rounded-t-xl max-h-44"
      />
      <p className="text-xl md:text-3xl font-circular-book text-gray-50 pt-5 pl-3">
        {' '}
        {category.name}{' '}
      </p>
      <p className="text-xs md:text-lg font-circular-light text-gray-50 pt-3 pl-3 pb-10">
        {category.views} Views
      </p>
    </Link>
  );
}
