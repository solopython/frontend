import { ICourseList } from '@/interfaces/courses/CoursesList';
import moment from 'moment';
import slugify from 'react-slugify';
import Link from 'next/link';
import StandardPagination from '../pagination/StandardPagination';

interface CoursesListProps {
  courses: ICourseList[];
  count: number;
  pageSize: number;
  currentPage: number;
  setCurrentPage: React.Dispatch<React.SetStateAction<number>>;
}

export default function CoursesList({
  courses,
  count,
  pageSize,
  currentPage,
  setCurrentPage,
}: CoursesListProps) {
  return (
    <div className="space-y-6">
      <div className="mx-auto mt-16 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-3">
        {courses?.map((course) => (
          <article key={course.id} className="flex flex-col items-start justify-between">
            <div className="relative w-full">
              <img
                src={course?.thumbnail}
                alt=""
                className="aspect-[16/9] w-full rounded-2xl bg-gray-100 object-cover sm:aspect-[2/1] lg:aspect-[3/2]"
              />
              <div className="absolute inset-0 rounded-2xl ring-1 ring-inset ring-gray-900/10" />
            </div>
            <div className="max-w-xl">
              <div className="mt-8 flex items-center gap-x-4 text-xs">
                <time dateTime={course?.updated_at} className="text-gray-500">
                  {moment(course?.updated_at).format('l')}
                </time>
                <Link
                  href={`/categories/view/${slugify(course?.category)}`}
                  className="relative z-10 rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100"
                >
                  {course.category}
                </Link>
              </div>
              <div className="group relative">
                <h3 className="mt-3 text-lg font-semibold leading-6 text-gray-900 group-hover:text-gray-600">
                  <Link href={`/courses/watch/${course?.slug}`}>
                    <span className="absolute inset-0" />
                    {course?.title}
                  </Link>
                </h3>
                <p className="mt-5 line-clamp-3 text-sm leading-6 text-gray-600">
                  {course?.short_description}
                </p>
              </div>
            </div>
          </article>
        ))}
      </div>
      <StandardPagination
        data={courses}
        count={count}
        pageSize={pageSize}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
      />
    </div>
  );
}
