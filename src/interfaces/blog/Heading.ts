import { IPost } from './Post';

export interface IHeading {
  post: IPost;
  title: string;
  slug: string;
  level: number;
}
