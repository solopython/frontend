/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  transpilePackages: ['@mercadopago/sdk-react'],
  typescript: {
    ignoreBuildErrors: true,
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  images: {
    domains: ['solopython-microservicios.s3.us-east-2.amazonaws.com'],
  },
};

module.exports = nextConfig;
